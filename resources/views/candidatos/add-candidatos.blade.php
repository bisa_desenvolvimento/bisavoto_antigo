@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')
<div class="content-admin">
    <h3>Candidato</h3>
    <form class="form-horizontal" method="post" action="{{ route(empty($cdt_id) ? 'salvar.candidato' : 'editar.candidato') }}" accept-charset="UTF-8" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="ele_id" value="{{$ele_id}}">
      <input type="hidden" name="car_id" value="{{$car_id}}">
      <input type="hidden" name="cdt_id" value="{{$cdt_id}}">
      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Nome do Candidato *</label>
            <input type="text" class="form-control" value="{{$cdt_nome}}" id="cdt_nome" name="cdt_nome" placeholder="Nome do Candidato*" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Número do Candidato *</label>
            <input type="text" class="form-control" value ="{{$cdt_numeroCandidatura}}" id="cdt_numeroCandidatura" name="cdt_numeroCandidatura" placeholder="Nº de Candidatura*" required="required">
        </div>
      </div>
     
     @if(strlen($cdt_fotocandidato)) 
    <div id="imgCandidato">
      <img src="/eleicoes/public/{{$cdt_fotocandidato}}" alt="{{$cdt_nome}}" width="145" height="145">
    </div>
    @endif
      
      <div class="form-group">
        <label for="cdt_fotocandidato" class="col-sm-2 control-label">Foto</label>
        <div class="col-sm-10">
            <input type="file" id="cdt_fotocandidato" value="{{$cdt_fotocandidato}}" name="cdt_fotocandidato" placeholder="Nome do Candidato*">
        </div>
      </div>
      <button type="submit" class="btn btn-default">Salvar</button>
    </form>

</div>
@endsection