@extends('app')
@section('content')
<script type="text/javascript" src="{{url('js/actionsEleitor.js')}}"></script>
    <div id="all">
        <div id="content">
                <div id="text">
                    <p>CANDIDATOS</p>
                </div><!-- /text -->
                <form name="frmCandidato" id="frmCandidato" method="post" action="{{route('salvar.votos')}}" ativo="1">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div style="display: none" id="votos"></div>
                    <?php
                        $xCargo = 1;
                    ?>
                    @foreach($listaCandidatos as $cargo)

                    <?php 
                        if ($cargo["car_restricao_zona"] == true && $zona_id != $cargo["car_zona_id"]) {
                            continue;
                        }
                    ?>

                    <div class="listaCargos" id="cargo{{$xCargo}}" <?php echo $xCargo == 1 ? '' : ' style="display: none"';?> votos="{{$cargo['car_votos']}}">
                        <p class="tituloEleicao" style="padding: 60px 0;">{{$cargo['car_nome']}}</p>

                            <?php
                                if($xCargo == 1){
                                    $txtVotacao = $cargo['car_votos'].' para '.$cargo['car_nome'];
                                }
                                $xCandidatos = 0;
                            ?>
                            
                            @if($_SESSION['eleicao']['exibir_sequenciaVotos'])
                            <div id="ttVotos">Votos <span class="quant_voto_atual">0</span> de <span style="color: #000 !important" class="txt_votacao">{{$txtVotacao}}</span></div>
                            @endif
                                        
                            <ul>
                                @foreach($cargo['candidatos'] as $candidato)
                              
                                <li class="<?php echo $xCandidatos % 4 == 0 ? 'clear ' : '';?>" id="card_candidato_{{$candidato['cdt_id']}}">
                                    <a href="javascript:;" class="candidato candidato{{$candidato['cdt_id']}}" dados="{{$candidato['cdt_id']}}|{{$candidato['cdt_nome']}}|{{$candidato['cdt_numeroCandidatura']}}|{{$cargo['car_id']}}">
                                        <div id="img">
                                            <?php
                                                $ftCandidato = "eleicoes/public/" . $candidato['cdt_fotocandidato'];
                                                // if(!file_exists('/'.$ftCandidato)):
                                                //     $ftCandidato = 'eleicoes/public/uploads/imgs/no-image.png';
                                                // endif;

                                                if(!$candidato['cdt_fotocandidato']):
                                                    $ftCandidato = 'eleicoes/public/uploads/imgs/no-image.png';
                                                endif;
                                            ?>
                                            <img src="/<?php echo $ftCandidato;?>" />
                                            <p>{{$candidato['cdt_numeroCandidatura']}}</p>
                                        </div>
                                        <p class="nome-candidato">{{$candidato['cdt_nome']}}</p>
                                    </a>
                                </li>
                                <?php
                                    $xCandidatos++;
                                ?>
                                @endforeach
                                
                            </ul>
                    </div>
                    <?php
                        $xCargo++;
                    ?>
                    @endforeach
                    <!-- <div id="ttVotos">Votos <span>0</span> de {{$txtVotacao}}</div> -->
                </form>

        </div><!-- /all -->
        <div id="botoes" class="clear">
                @if ($_SESSION['eleicao']['exibir_branco'] == 1)
                    <a href="javascript:;" id="branco" class="opcCandidato branco" dados="99|Voto em Branco|99">branco</a>
                @endif
                @if ($_SESSION['eleicao']['exibir_nulo'] == 1)
                    <a href="javascript:;" id="nulo" class="opcCandidato nulo" dados="98|Voto Nulo|98">nulo</a>
                @endif
            </div> <!-- /botoes -->

    </div><!-- /all -->
    <script type="text/javascript">
        $(document).ready(function(){

            $("#modOk").hide();

            oab = '';
            votos = 0;

            $('.candidato, .opcCandidato').click(function(){
                $("#imgGrande").find("img").attr("src", '');
                dados = $(this).attr('dados').split('|');
                console.log(dados)
                oab = dados[0];

                var numeroCandidato = dados[2];
                var nomeCandidato = dados[1];

                if (numeroCandidato == "98"){
                    numeroCandidato = "&nbsp;";
                } else if (numeroCandidato == 99){
                    numeroCandidato = "&nbsp;";
                }
                $('#numeroCandidato').empty().html('<strong>'+nomeCandidato+'</strong>');
                var imagem;
                if(oab == '99') {
                    imagem = "{{url('img/fotos/branco.jpg')}}";
                } else if(oab == '98') {
                    imagem = "{{url('img/fotos/nulo.jpg')}}";
                } else {
                    imagem = $(".candidato"+oab).find("img").attr("src");
                }
                $("#imgGrande").find("img").attr("src", imagem);
                $("#imgGrande").find("img").attr("style", "max-width: 256px; max-width: 256px;");

                $("#frmCandidato").attr("cargo", dados[3]);

                showModal();
            });

            $('#corrige').click(function(){
                closeModal();
            })

            $('#confirma').click(function(){

                var voto = '<input type="hidden" name="votos[]" value="'+oab+'" />';
                // voto += '<input type="hidden" name="cargo[]" value="'+$("#frmCandidato").attr("cargo")+'" />';

                if (oab == "98" || oab == "99") {
                    let myI = $("#frmCandidato").attr("ativo");
                    let myTarget = $(`#cargo${myI} ul li`)[0];
                    let myCargo = $(myTarget).children().attr("dados").split('|')[3];

                    voto += '<input type="hidden" name="cargo[]" value="'+ myCargo +'" />';
                } else {
                    voto += '<input type="hidden" name="cargo[]" value="'+$("#frmCandidato").attr("cargo")+'" />';
                }
 

                //Thiago 
                
                $('#votos').append(voto);

                var qtdVotos = $("#votos").find("input").length;
                votos++;
                var votacaoAtiva = $("#frmCandidato").attr("ativo");
                votacaoAtiva = parseInt(votacaoAtiva);

                var ttVotos = $("#cargo"+votacaoAtiva).attr("votos");
                ttVotos = parseInt(ttVotos);
                

                $("#cargo"+votacaoAtiva).find('li').each(function()
                {
                    dados = $(this).find('a').attr('dados').split('|');
                    
                    if(votos < ttVotos){

                        if(dados[0] == oab) {
                            $(this).removeClass('candidato');
                            $(this).unbind('click');
                            $(`#card_candidato_${oab}`).hide();
                        } 

                    } else {
                        if(dados[0] == oab) {
                            $(this).removeClass('candidato');
                            $(this).unbind('click');
                        } else {
                            $(`#card_candidato_${dados[0]}`).hide();
                         }   
                    }
                } )
                
                closeModal();

                if(votos >= ttVotos) {
                    if (votacaoAtiva == $(".listaCargos").length){
                        $('.candidato').each(function(){
                             $(this).unbind('click');
                        });
                        $('.opcCandidato').each(function(){
                             $(this).unbind('click');
                        });

                        $('#frmCandidato').submit();
                    } else {
                        votacaoAtiva++;
                        votos = 0;
                        $("#frmCandidato").attr("ativo", votacaoAtiva);
                        votacaoAtiva = $("#frmCandidato").attr("ativo");
                        $(".listaCargos").hide();
                        $("#cargo"+votacaoAtiva).show();
                    }

                }

                var txtVotacao = '';

                if (ttVotos > 1){
                    txtVotacao = " para "+$("#cargo"+votacaoAtiva).find('.tituloEleicao').html();
                }
                // console.log("votos:" + votos)
                // console.log("Total Votos:" + ttVotos)
                // console.log("Texto:" + txtVotacao)
                // <div id="ttVotos">Votos <span class="quant_voto_atual">0</span> de <i class="txt_votacao">{{$txtVotacao}}</i></div>
                // $("#ttVotos").html("Votos <span>" + votos + "</span> de "+ttVotos+txtVotacao);

                $(".quant_voto_atual").text(votos);
                $(".txt_votacao").text(`${ttVotos}${txtVotacao}`);

            });

            document.onkeydown = checkKeycode
                function checkKeycode(e) {
                    var keycode;
                    if (window.event)
                        keycode = window.event.keyCode;
                    else if (e)
                        keycode = e.which;

                    // Mozilla firefox
                    if ($.browser.mozilla) {
                        if (keycode == 116 ||(e.ctrlKey && keycode == 82)) {
                            if (e.preventDefault)
                            {
                                e.preventDefault();
                                e.stopPropagation();
                            }
                        }
                    }
                    // IE
                    else if ($.browser.msie) {
                        if (keycode == 116 || (window.event.ctrlKey && keycode == 82)) {
                            window.event.returnValue = false;
                            window.event.keyCode = 0;
                            window.status = "Refresh is disabled";
                        }
                    }
                }

        });

        var mensagem="";
        function clickIE() {
                if (document.all)
                {
                    (mensagem);
                    return false;
                }
            }
        function clickNS(e) {
            if (document.layers||(document.getElementById&&!document.all))
            {
                if (e.which==2||e.which==3)
                {
                    (mensagem);
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;
        } else {
            document.onmouseup=clickNS;document.oncontextmenu=clickIE;
        }
        document.oncontextmenu = new Function("return false")

        var tempo = new Number();
        // Tempo em segundos
        tempo = <?php echo Session::get('dataEleicao')->ele_tempo;?>;

        function startCountdown(){

            // Se o tempo nÃ£o for zerado
            if((tempo - 1) >= 0){

                // Pega a parte inteira dos minutos
                var min = parseInt(tempo/60);
                // Calcula os segundos restantes
                var seg = tempo%60;

                // Formata o nÃºmero menor que dez, ex: 08, 07, ...
                if(min < 10){
                    min = "0"+min;
                    min = min.substr(0, 2);
                }
                if(seg <=9){
                    seg = "0"+seg;
                }

                // Cria a variÃ¡vel para formatar no estilo hora/cronÃ´metro
                horaImprimivel = '00:' + min + ':' + seg;
                //JQuery pra setar o valor
                $("#ur").html(horaImprimivel);

                // Define que a funÃ§Ã£o serÃ¡ executada novamente em 1000ms = 1 segundo
                setTimeout('startCountdown()',1000);

                // diminui o tempo
                tempo--;

            // Quando o contador chegar a zero faz esta aÃ§Ã£o
            } else {
               // location.href='/auth/logout';
            }

        }
        startCountdown();
    </script>
@endsection