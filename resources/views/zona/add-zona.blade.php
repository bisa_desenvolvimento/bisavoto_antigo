@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')
<div class="content-admin">
    <h3>Zona Eleitoral</h3>
    <form class="form-horizontal" method="post" action="{{route(empty($id) ? 'salvar.zona' : 'update.zona')}}">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="ele_id" value="{{$ele_id}}">
      <input type="hidden" name="id" value="{{$id}}">
      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Nº da Zona Eleitoral *</label>
            <input type="text" class="form-control" id="zon_numero" name="zon_numero" value="{{$zon_numero}}" placeholder="Nº da Zona Eleitoral *" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Nome da Zona Eleitoral *</label>
            <input type="text" class="form-control" id="zon_nome" value="{{$zon_nome}}" name="zon_nome" placeholder="Nome da Zona Eleitoral *" required="required">
        </div>
      </div>
      <button type="submit" class="btn btn-default">Salvar</button>
    </form>

</div>
@endsection