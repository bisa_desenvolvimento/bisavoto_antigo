@extends('app')
@section('content')

<div id="tela04">

	<div id="votacaoTopo">

    	<p>Eleições<br>@if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo)
        {{ $_SESSION['eleicao']->ele_nome }}
        @endif</p>

    </div>

    <div id="formVotacao">
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)

            <script type="text/javascript">

                $(document).ready(function(){
                    modal('alert','{{ $error }}');
                });

            </script>
            @endforeach
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ secure_asset('/auth/login') }}">
        						<input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(isset($eleicao))
                <input type="text" class="validate[required] left" name="matricula" value="{{ old('matricula') }}" placeholder="Login">
                <input type="hidden" name="ele_id" value="{{ $eleicao->ele_id }}">
            @else
                <input type="text" class="validate[required] left" name="cpf" value="{{ old('cpf') }}" placeholder="Login">
            @endif

            <!--<input type="password" name="senha" id="senha" class="validate[required] left clear" value="Senha" />-->
            <input type="password" class="validate[required] left clear" name="password" placeholder="Senha">

            <div class="clear"></div>

            <button type="submit" class="login clear" onclick="document.formLoginUsuario.submit();">login</button>
            
        </form>
        @if(isset($eleicao))
            <a href="{{ route('link.eleicao.linkReenviarSenha', ['alias' => $eleicao->alias]) }}" style="font-size: 20px;color: #eb240f;">Esqueceu a senha? Clique Aqui</a>
        @endif
    </div>

</div><!-- /tela04 -->

@endsection

