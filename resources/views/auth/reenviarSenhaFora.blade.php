@extends('app')
@section('content')
<div id="tela04">

	<div id="votacaoTopo">

    	<p>Eleições</p>

    </div>
    
    <div id="formVotacao">
        <span style="font-size: 20px;margin-bottom: 30px;display: inline-flex;">Preencha os campos abaixo para enviar-mos uma nova senha</span>
        <form id="formReenvioSenha" class="form-horizontal" role="form" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{-- <input type="hidden" name="ele_id" id value=""> --}}

            <input type="text" class="left clear" name="nome" placeholder="Nome*" required>
            <input type="email" class="left clear" name="email" placeholder="E-mail*" required>
            <input type="text" class="left clear" name="matricula" placeholder="Matricula*" required>
            <input type="text" class="left clear" name="cpf" placeholder="CPF*" required>
            <input type="text" class="left clear" name="telefone" placeholder="Telefone*" required>
            <div class="clear"></div>
            <span style="font-size: 20px;">Selecione a eleição</span>
            <div class="clear" style="margin-bottom: 60px;"></div>
            <?php $count = 0; ?>
            @if ($eleicoes)
                @foreach ($eleicoes as $key => $item)
                    @if (date("Y-m-d H:i:s") <= $item->ele_horaTermino)
                        <?php $count++; ?>
                        <div class="left clear" style="margin-left:60px;margin-bottom: 40px;">
                            <input style="width: 50px;margin-bottom: -50px;" type="radio" id="eleicao-{{ $key }}" data-idEleicao="{{ $item->ele_id }}" name="ele_id" value="{{ $item->ele_id }}" required>
                            <span style="position: absolute;margin-top: -25px;font-size: 18px">{{ $item->dataInicio }}</span>
                            <label class="rigth clear" for="eleicao-{{ $key }}"><img src="{{ secure_asset($item->ele_logo) }}" width="180" alt=""  /></label><br>
                        </div>
                    @endif
                @endforeach
            @endif
            @if ($count == 0)
                <span style="font-size: 20px;">Nenhuma eleição encontrada pra hoje</span>
            @endif
            <div class="clear"></div>
            @if ($eleicoes)
                @if ($count > 0)
                    <div id="btReenviarSenha">
                        <input style="background: url({{ secure_asset('img/button_reenviar-senha.png') }}) no-repeat;width: 290px;height: 66px;display: inline-block;text-indent: -9999px;margin: 70px 0 40px;border: none;cursor: pointer;" type="submit" class="login clear" value="Reenviar Senha">
                    </div>
                @endif
            @endif
        </form>
    </div>

</div><!-- /tela04 -->

<script>
    
    $(document).on('submit', '#formReenvioSenha', function (event) {
        event.preventDefault();
        $.ajax({
            url: "{{ route('link.eleicao.reenviarsenha') }}", 
            type: "POST",
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend : function(){
                $("#btReenviarSenha").html(`<img src="{{ secure_asset('img/load.gif') }}" style="width: 50px;">`);
            },
            success: function(data){
                switch (data.resultado) {
                    case 'envia_email':
                        modal('alert','Uma nova senha foi gerada e enviada para o e-mail informado!');
                        break;
                    case 'email_incorreto':
                        modal('alert','Com base nas informações passadas o seu e-mail cadastrado é: <span style="color:red;">'+data.emailCorreto+'</span> entre em contato com a Comissão eleitoral para atualizar seu e-mail.');
                        break;
                    case 'inconsistencia':
                        modal('alert','Encontramos inconsistências nos dados informados, por favor entre em contato com a Comissão eleitoral para verificar seus dados.');
                        break;
                }
                $("#btReenviarSenha").html(`<input style="background: url({{ secure_asset('img/button_reenviar-senha.png') }}) no-repeat;width: 290px;height: 66px;display: inline-block;text-indent: -9999px;margin: 70px 0 40px;border: none;cursor: pointer;" type="submit" class="login clear" value="Reenviar Senha">`);
            },
            error: function(request, status, error) {
                // faço a chamada do modal aq pq o data, está como json quebrado por causa do retorno do envio do email
                modal('alert','Uma nova senha foi gerada e enviada para o e-mail informado!');
                $("#modOk").attr('href', '');
                $("#btReenviarSenha").html('');
            }
        });
    });
</script>
@endsection