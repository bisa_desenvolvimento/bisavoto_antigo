@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
		margin-bottom: 50px;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Lista de votantes</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

@foreach($votantesPorZona as $zona => $votantes)
	<div class="text-center">
		<h3 class="text-center"></h3>
		<table class="table table-striped" cellspacing="0" cellpadding="0">
		  <thead>
		    <tr>
		      <th class="col" colspan="2">{{ $zona }}</th>
		    </tr>
		    <tr>
		      <th class="col">Nome</th>
		      <th class="col">Zona</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($votantes as $votante)
		    <tr>
		      <td class="col">{{ $votante->Nome }}</td>
		      <td class="col text-center">{{ $votante->Zona }}</td>
		    </tr>
		    @endforeach
		    <tr>
		    	<th class="row">TOTAL DE VOTANTES DESSA ZONA</th>
		    	<th class="text-center">{{ count($votantes) }}</th>
		    </tr>
		  </tbody>
		</table>
	</div>
@endforeach
@endsection