@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<?php //dd($votantes); ?>

<div class="text-center">
	<h1>Lista de votantes</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

<div class="text-center">
	<table class="table table-striped" cellspacing="0" cellpadding="0">
	  <thead>
	    <tr>
	      <th class="col">Nome</th>
	      <th class="col">Zona</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($votantes as $votante)
	    <tr>
	      <td>{{ $votante->nome }}</td>
	      <td>{{ $votante->zona }}</td>
	    </tr>
	    @endforeach
	    <tr>
	    	<th class="row">TOTAL DE VOTANTES</th>
	    	<th class="text-center">{{ count($votantes) }}</th>
	    </tr>
	  </tbody>
	</table>
</div>
@endsection