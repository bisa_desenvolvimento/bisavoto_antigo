@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Lista de Aptos para Votação</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

<div class="text-center">
	<table class="table table-striped" cellspacing="0" cellpadding="0">
	  <thead>
	    <tr>
	      <th class="col">Matrícula</th>
	      <th class="col">Nome</th>
	      <th class="col">Email</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($aptos as $apto)
	    <tr>
	      <th class="row">{{ $apto->matricula }}</th>
	      <td>{{ $apto->nome }}</td>
	      <td>{{ $apto->email }}</td>
	    </tr>
	    @endforeach
	    <tr>
	    	<td></td>
	    	<th class="row">TOTAL DE APTOS</th>
	    	<th class="text-center">{{ count($aptos) }}</th>
	    </tr>
	  </tbody>
	</table>
</div>
@endsection