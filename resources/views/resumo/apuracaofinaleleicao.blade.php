@extends('resumo')
@section('content')
<!-- Novo Layout -->

<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	img {
		margin-bottom: 15px;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
		margin-bottom: 30px;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<?php //dd(count($aptos)); ?>

<div class="text-center">
	<h1>Resultado Final da Votação</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>
	<h4>Inicio da Votação: {{ date("H:i d/m/Y", strtotime($eleicao->ele_horaInicio)) }}</h4>
	<h4>Fim da Votação: {{ date("H:i d/m/Y", strtotime($eleicao->ele_horaTermino)) }}</h4>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

		<h2>Total de Aptos: {{ count($aptos) }}</h2>

	</div><!-- /logo -->
</div>
@foreach($votos['candidatos'] as $votosCargo)
	<?php $totalVotos = 0; ?>
	<label>{{ $votosCargo[0]->car_nome }}</label>
	<div class="text-center">
		<table class="table table-striped" cellspacing="0" cellpadding="0">
		  <thead>
		    <tr>
		      <th class="col">Nº Cadidatura</th>
		      <th class="col">Nome</th>
		      <th class="col">Quant Votos</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($votosCargo as $voto)
			  	@if ($voto->cdt_numeroCandidatura == 99 || $voto->cdt_numeroCandidatura == 98)
					@if ($voto->cdt_numeroCandidatura == 99)
						@if ($eleicao->exibir_branco == 1)
							<tr>
								<th class="row">{{ $voto->cdt_numeroCandidatura }}</th>
								<td>{{ $voto->cdt_nome }}</td>
								<td class="text-center">{{ $voto->votos }}</td>
								<?php $totalVotos += $voto->votos; ?>
							</tr> 
						@endif
					@endif
					@if ($voto->cdt_numeroCandidatura == 98)
						@if ($eleicao->exibir_nulo == 1)
							<tr>
								<th class="row">{{ $voto->cdt_numeroCandidatura }}</th>
								<td>{{ $voto->cdt_nome }}</td>
								<td class="text-center">{{ $voto->votos }}</td>
								<?php $totalVotos += $voto->votos; ?>
							</tr> 
						@endif
					@endif
				@else
					<tr>
						<th class="row">{{ $voto->cdt_numeroCandidatura }}</th>
						<td>{{ $voto->cdt_nome }}</td>
						<td class="text-center">{{ $voto->votos }}</td>
						<?php $totalVotos += $voto->votos; ?>
					</tr>
				@endif
		    @endforeach
		    <tr>
		    	<td></td>
		    	<th class="row">TOTAL DE VOTOS</th>
		    	<th class="text-center">{{ $totalVotos }}</th>
		    </tr>
		  </tbody>
		</table>
	</div>
@endforeach
@endsection