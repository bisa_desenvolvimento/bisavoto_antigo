@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
		margin-bottom: 50px;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Relatório de Ocorrências</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

	<div class="text-center">
		<h3 class="text-center"></h3>
		<table class="table table-striped" cellspacing="0" cellpadding="0">
		  <thead>
		    <tr>
				<th class="col">Dia</th>
				<th class="col">Hora</th>
				<th class="col">Usuário</th>
				<th class="col">Login</th>
				<th class="col">Ocorrência</th>
				<th class="col" style="text-align: left;">Detalhes</th>
				<th class="col">IP|Navegador</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($dados as $key => $dado) 
			<?php 
			  	$ocor = explode("::", $dado->Log_Operacao); 
			  	$operacao = $ocor[1];
				
				/** Este array guarda as operações com necessidade de exibir os dados do campo Log_DadosRegistro no relatório. */
				$operacoes = [
					"ENVIO EMAIL INDIVIDUAL",
					"ENVIO DE EMAILS GERAL",
					"ENVIO DE EMAILS E GERAÇÃO DE SENHA",
					"GERAÇÂO DE SENHA SEM ENVIO DE EMAIL",
					"ALTERAÇÂO DE EMAIL",
					"INCLUSÃO DE ELEITOR",
					"EXCLUSÃO DE ELEITOR",
					"ALTERAÇÃO DE NOME DE ELEITOR",
					"ALTERAÇÃO DE MATRÍCULA DE ELEITOR",
					"ALTERAÇÃO DE CPF DE ELEITOR",
					"ALTERAÇÃO DE ZONA ELEITORAL DE ELEITOR"
					
				];
			?>    
		    <tr>
				<td class="col" style="text-align: center;">{{ \Carbon\Carbon::parse($dado->Log_Data)->format('d/m/Y') }}</td>
				<td class="col" style="text-align: center;">{{ \Carbon\Carbon::parse($dado->Log_Data)->format('H:i') }}</td>
				<td class="col" style="text-align: center;">{{ $dado->name }}</td>
				<td class="col" style="text-align: center;">{{ $dado->login }}</td>
				<td class="col" style="text-align: center;">{{ $operacao }}</td>
				@if (in_array($operacao, $operacoes))
					<td class="col" style="text-align: left;">
						<?php
							$dado->Log_DadosRegistro = str_replace("\'","'",$dado->Log_DadosRegistro); 
							$arrayDadosRegistro = json_decode($dado->Log_DadosRegistro);

							if($operacao == 'GERAÇÂO DE SENHA SEM ENVIO DE EMAIL'){
								unset($arrayDadosRegistro->id);
							}
							
						?>
						@foreach ($arrayDadosRegistro as $key => $item)
							<b>{{ $key }}</b>: {{ $item }}<br>
						@endforeach 
					</td>
				@else
					<td class="col" style="text-align: center;">-</td>
				@endif
				<td class="col" style="text-align: center;">{{ explode(" ", $dado->Log_DadosAdicionais)[0] }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	</div>
@endsection