@extends('resumo')
@section('content')
<!-- Novo Layout -->

<style>
    body {
        font-family: Helvetica, sans-serif;
        font-weight: normal;
        font-style: normal;
    }

    img {
        margin-bottom: 15px;
    }

    .text-center {
        text-align: center
    }

    .table {
        width: 100%;
        margin-bottom: 1px;
    }

    .table .col {
        padding: 5px;
    }

    .table thead {
        background: #ccc;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    @media print { 
        #btImprimir { display:none; } 
    }

</style>

<?php //dd(count($aptos));
?>
<input type="button" id="btImprimir" value="Imprimir" onClick="window.print()"  style="width: 90px;height: 30px;font-size: 15px;cursor: pointer;">
<div class="text-center">
    <h1>Resultado Final da Votação por Zona Eleitoral</h1>
    <h2>{{ $eleicao->ele_nome }}</h2>
    <h4>Inicio da Votação: {{ date('H:i d/m/Y', strtotime($eleicao->ele_horaInicio)) }}</h4>
    <h4>Fim da Votação: {{ date('H:i d/m/Y', strtotime($eleicao->ele_horaTermino)) }}</h4>

    <div id="logoPrincipal2">
        <?php
        $url = url('img/logo/nova-marca-cremepe.png');
        if (isset($eleicao) && $eleicao->ele_logo) {
            $url = url($eleicao->ele_logo);
        }
        
        ?>
        <img src="{{ $url }}" width="180" alt="" />

        <h2>Total de Aptos: {{ count($aptos) }}</h2>

    </div><!-- /logo -->
</div>

<div class="text-center">

    <?php $contBrancoNulo = 0; ?>
    @foreach ($cargos as $cargo)
        <table class="table table-striped" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="col" style="text-align: left;">Cargo: {{ $cargo->car_nome }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cargo->zona as $zona)
                    <table class="table table-striped" cellspacing="0" cellpadding="0" style="padding-left: 50;">
                        <thead>
                            <tr>
                                <th class="col" style="text-align: left;">Zona: {{ $zona->zon_nome }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <table class="table table-striped" cellspacing="0" cellpadding="0" style="padding-left: 100;">
                                <thead>
                                    <tr>
                                        <th class="col" style="width:700px;text-align: left;">Candidatos</th>
                                        <th class="col" style="width:500px">Qdt Votos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($zona->candidatos as $candidato)
                                        <tr>
                                            <td>{{ $candidato->cdt_nome }}</td>
                                            <td class="text-center">{{ $candidato->votos }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Brancos</td>
                                        <td class="text-center">{{ $zona->votos_branco }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nulos</td>
                                        <td class="text-center">{{ $zona->votos_nulo }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </tbody>
                    </table>
                @endforeach
            </tbody>
        </table>
    @endforeach

</div>
@endsection
