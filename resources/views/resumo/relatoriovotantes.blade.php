@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Resultado de número de votantes</h1>
	<h2>{{ $eleicao->ele_nome }}</h2>
	<h2>Aptos a votar: {{ $aptos }}</h2>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

<div class="text-center">
	<table class="table table-striped mt-2" cellspacing="0" cellpadding="0">
	  <thead>
	    <tr>
	      <th class="col">Dia</th>
	      <th class="col">Hora</th>
	      <th class="col">Nº Votantes</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th class="row">{{ date('d/m/Y') }}</th>
	      <td class="text-center">{{ date('H:i') }}</td>
	      <td class="text-center">{{ count($votantes) }}</td>
	    </tr>

	  </tbody>
	</table>
</div>
@endsection