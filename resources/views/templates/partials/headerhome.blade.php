<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>
    @if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo)
        {{ $_SESSION['eleicao']->ele_nome }}
    @else
        {{"Eleições Online"}}
    @endif
    </title>

    <script type="text/javascript" src="{{secure_asset('js/jquery-1.8.0.min.js')}}"></script>
	<script type="text/javascript" src="{{secure_asset('js/sawpf.1.0.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/lightbox/ekko-lightbox.min.js')}}"></script>

     <script type="text/javascript" src="{{secure_asset('js/actions.js')}}"></script>

    <link href="{{secure_asset('css/reset.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('css/screen.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('js/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="{{ secure_asset('img/logo/logoBisa.png') }}">
    <!--
    <script type="text/javascript">
        function UR_Start()
        {
        	UR_Nu = new Date;
        	UR_Indhold = showFilled(UR_Nu.getHours()) + ":" + showFilled(UR_Nu.getMinutes()) + ":" + showFilled(UR_Nu.getSeconds());
        	document.getElementById("ur").innerHTML = UR_Indhold;
        	setTimeout("UR_Start()",1000);
        }
        function showFilled(Value)
        {
        	return (Value > 9) ? "" + Value : "0" + Value;
        }
    </script>
    -->
    <script type="text/javascript">
        var url = "{{ secure_asset('/') }}";
        var referer = "<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : secure_asset('/'); ?>";
    </script>
</head>

<body class="int2">

    <div id="modal" style="display: none;">
    	<div class="sombra"></div>
        <div id="alert">
        	<div id="imgGrande" class="left">
                <img src="" alt="" />
            </div><!-- /imgGrande -->
            <div id="confirmar" >
            	<p class="numero" id="numeroCandidato"><strong></strong></p>
            	<p class="nome" id="modMensagem"><strong></strong></p>
                <p class="nome" id="nomeCandidato"><strong></strong></p>
                <a href="javascript:;" id="confirma" class="confirma">confirma</a>
            	<a href="javascript:;" id="corrige" class="corrige">corrige</a>
            </div><!-- /confirmar -->
            <div id="modAlert">
                <p class="nome modMensagem"><strong></strong></p>
                <a href="javascript:;" onclick="closeModal()" id="modOk" class="modOk" style="margin-top: 100px">ok</a>
            </div>
            <!-- /confirmar -->
        </div><!--/alert-->
    </div><!-- /modal -->

    <span id="ur"></span>

	<div id="all">

    	<div id="logoPrincipal2">
            <?php
                $url = secure_asset('img/logo/nova-marca-cremepe.png');
                if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo) {
                    $url = secure_asset($_SESSION['eleicao']->ele_logo);
                }

            ?>
            <img src="{{ $url }}" width="180" alt=""  />

        </div><!-- /logo -->

        <div id="content3" class="clear">

            <div id="bgTop">
            </div>

            <div id="bgRight">
            </div>

            <div id="bgLeft">
            </div>

            <div id="bgBottom">
            </div>
