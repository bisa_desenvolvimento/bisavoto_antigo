<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>
    @if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo)
        {{ $_SESSION['eleicao']->ele_nome }}
    @else
        {{"Eleições Online"}}
    @endif
    </title>

    <script type="text/javascript" src="{{secure_asset('js/jquery-2.1.4.min.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/jquery.csv-0.71.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{secure_asset('js/timepicker/jquery-ui-timepicker-addon.js')}}"></script> -->
		<script type="text/javascript" src="{{secure_asset('js/timepicker/jquery-ui-timepicker-addon.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/sawpf.1.0.js')}}"></script>
	  <link rel="stylesheet" href="{{secure_asset('js/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />

    <script type="text/javascript" src="{{secure_asset('js/actions.js')}}"></script>
		<script type="text/javascript" src="{{secure_asset('js/jquery.mask.js')}}"></script>

    <link href="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.theme.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="{{ secure_asset('img/logo/logoBisa.png') }}">
    <!-- <link href="{{secure_asset('js/timepicker/jquery-ui-timepicker-addon.css')}}" rel="stylesheet" type="text/css" media="all" /> -->

    <link href="{{secure_asset('css/reset.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('css/screen.css')}}" rel="stylesheet" type="text/css" media="all" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="{{secure_asset('js/jAlert-v3.js')}}" type="text/javascript"></script>
    <script src="{{secure_asset('js/jAlert-functions.js')}}" type="text/javascript"></script>
    <link href="{{secure_asset('css/jAlert-v3.css')}}" rel="stylesheet" type="text/css" media="screen" />

		<script type="text/javascript" src="{{secure_asset('js/bower_components/moment/min/moment.min.js')}}"></script>
		<script type="text/javascript" src="{{secure_asset('js/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

    <?php
    if(isset($_GET['download'])):
    ?>
        <script type="text/javascript">
            var urlDownload = window.location.href;
            var validUrl = urlDownload.split('?');
            validUrl = validUrl[0];
            setTimeout("redirectToUrl()", 3000);
            location.href = '<?php echo $_GET['download'];?>';
        </script>
    <?php
    endif;
    if(isset($_GET['msgExclusao'])):
        $msgExclusao = $_GET['msgExclusao'];
        if(strstr($msgExclusao, 'erro', true)):
    ?>
            <script type="text/javascript">
                $(
                    function()
                    {
                        errorAlert('Erro', '<?php echo $msgExclusao;?>');
                    }
                )
            </script>
    <?php
        else:
    ?>
            <script type="text/javascript">
                $(
                    function()
                    {
                        successAlert('Sucesso', '<?php echo $msgExclusao;?>');
                    }
                )
            </script>
    <?php
        endif;

    endif;
    ?>

</head>

<body class="int2">

    <div id="modal" style="display: none;">
    	<div class="sombra"></div>
        <div id="alert">
        	<div id="confirmar" >
            	<p class="numero" id="numeroCandidato"><strong></strong></p>
                <p class="nome" id="modMensagem"><strong></strong></p>
                <a href="javascript:;" id="modOk" class="modOk modAlert">ok</a>
                <a href="javascript:;" id="modConfirma" class="modConfirma modConfirm">confirma</a>
            	<a href="javascript:;" id="modCancela" class="modCancela modConfirm">cancela</a>
            </div><!-- /confirmar -->
        </div><!--/alert-->
    </div><!-- /modal -->

    <div id="modalErro" style="display: none;">
        <div class="sombraErro"></div>
        <div id="alertErro">
            <div id="confirmarErro" >
                <p class="nome" id="modMensagemErro"><strong></strong></p>
                <a href="javascript:;" id="modOkErro" class="modOkErro modAlertErro" style="margin-top: 100px;">ok</a>
            </div><!-- /confirmar -->
        </div><!--/alert-->
    </div><!-- /modal -->

	<div id="all">

    	<div id="logoPrincipal2">

            <img src="{{secure_asset('img/logo/logoBisa.png')}}" alt=""  />

        </div><!-- /logo -->

        <div id="content3" class="clear">

            <div id="bgTop">
            </div>

            <div id="bgRight">
            </div>

            <div id="bgLeft">
            </div>

            <div id="bgBottom">
            </div>
            <br /><br /><br /><br />

            <?php
                $nameRoute = Route::currentRouteName();

                $activeEleicao = '';
                $activeEleitor = '';
                $activeCargo = '';
                $activeCandidatos = '';
                $activeZona = '';
                $links = '';
                switch($nameRoute){
                    case 'lista.eleicoes':
                        $activeEleicao = ' class="active"';
                        break;
                    case 'lista.eleitores':
                        $activeEleitor = ' class="active"';
                        $url = route('lista.eleitores', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Eleitores</a></li>';
                        break;
                    case 'notificar.naonotificados':
                        $url = route('lista.eleitores', Request::segment(3));
                        $links = '<li><a href="'.$url.'">Eleitores</a></li>';
                        break;
                    case 'add.eleitor':
                        $activeEleitor = ' class="active"';
                        $url = route('lista.eleitores', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Eleitores</a></li>';
                        break;
                    case 'lista.cargos':
                        $activeCargo = ' class="active"';
                        $url = route('lista.cargos', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Cargos</a></li>';
                        break;
                    case 'add.cargos':
                        $activeCargo = ' class="active"';
                        $url = route('lista.cargos', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Cargos</a></li>';
                        break;
                    case 'lista.candidatos':
                        $activeCandidatos = ' class="active"';
                        $url = route('lista.cargos', Request::segment(5));
                        $links = '<li><a href="'.$url.'">Cargos</a></li>';
                        $url = route('lista.candidatos', [Request::segment(3), Request::segment(5)]);
                        $links .= '<li class="active"><a href="'.$url.'">Candidatos</a></li>';
                        break;
                    case 'add.candidato':
                        $activeCandidatos = ' class="active"';
                        $url = route('lista.cargos', Request::segment(5));
                        $links = '<li><a href="'.$url.'">Cargos</a></li>';
                        $url = route('lista.candidatos', [Request::segment(3), Request::segment(5)]);
                        $links .= '<li class="active"><a href="'.$url.'">Candidatos</a></li>';
                        break;
                    case 'edit.candidato':
                        $activeCandidatos = ' class="active"';
                        $url = route('lista.cargos', Request::segment(5));
                        $links = '<li><a href="'.$url.'">Cargos</a></li>';
                        $url = route('lista.candidatos', [Request::segment(3), Request::segment(5)]);
                        $links .= '<li class="active"><a href="'.$url.'">Candidatos</a></li>';
                        break;
                    case 'lista.zonas':
                        $activeZona = ' class="active"';
                        $url = route('lista.zonas', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Zona Eleitoral</a></li>';
                        break;
                    case 'add.zona':
                        $activeZona = 'class="active"';
                        $url = route('lista.zonas', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Zona Eleitoral</a></li>';
                        break;
                    case 'lista.comissao':
                        $activeZona = ' class="active"';
                        $url = route('lista.comissao', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Comissão Eleitoral</a></li>';
                        break;
                    case 'add.comissao':
                        $activeZona = 'class="active"';
                        $url = route('lista.comissao', Request::segment(3));
                        $links = '<li class="active"><a href="'.$url.'">Comissão Eleitoral</a></li>';
                        break;
                    case 'resumo':
                        $activeEleicao = ' class="active"';
                        break;
                    default;
                }
            ?>
						<script type="text/javascript">
							var nameRoute = '<?php echo $nameRoute;?>';
						</script>
            <ol class="breadcrumb">
                <?php if ($nameRoute == 'pagina.eleicao') { ?>
                    <li{{$activeEleicao}}><a href="{{route('lista.eleicoes')}}">Eleições</a></li>
                <?php } else { ?>
                    <li{{$activeEleicao}}><a href="{{route('pagina.eleicao',[Session::get('ele_id')])}}">Eleição</a></li>
                <?php } ?>
              <?php echo $links;?>
              <li><a href="{{route('user.logout')}}">Sair</a></li>
            </ol>
