<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>
    @if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo)
        {{ $_SESSION['eleicao']->ele_nome }}
    @else
        {{"Eleições Online"}}
    @endif
    </title>

    <script type="text/javascript" src="{{secure_asset('js/jquery-2.1.4.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/jquery.csv-0.71.min.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/timepicker/jquery-ui-timepicker-addon.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/sawpf.1.0.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/actions.js')}}"></script>

    <link href="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.theme.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('js/timepicker/jquery-ui-timepicker-addon.css')}}" rel="stylesheet" type="text/css" media="all" />


    <link href="{{secure_asset('css/reset.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{secure_asset('css/screen.css')}}" rel="stylesheet" type="text/css" media="all" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="{{secure_asset('js/jAlert-v3.js')}}" type="text/javascript"></script>
    <script src="{{secure_asset('js/jAlert-functions.js')}}" type="text/javascript"></script>
    <link href="{{secure_asset('css/jAlert-v3.css')}}" rel="stylesheet" type="text/css" media="screen" />

    <?php
    if(isset($_GET['msgExclusao'])):
        $msgExclusao = $_GET['msgExclusao'];
        if(strstr($msgExclusao, 'erro', true)):
    ?>
            <script type="text/javascript">
                $(
                    function()
                    {
                        errorAlert('Erro', '<?php echo $msgExclusao;?>');
                    }
                )
            </script>
    <?php
        else:
    ?>
            <script type="text/javascript">
                $(
                    function()
                    {
                        successAlert('Sucesso', '<?php echo $msgExclusao;?>');
                    }
                )
            </script>
    <?php
        endif;

    endif;
    ?>

</head>

<body class="int2">

    <div id="modal" style="display: none;">
    	<div class="sombra"></div>
        <div id="alert">
        	<div id="confirmar" >
            	<p class="numero" id="numeroCandidato"><strong></strong></p>
                <p class="nome" id="modMensagem"><strong></strong></p>
                <a href="javascript:;" id="modOk" class="modOk modAlert">ok</a>
                <a href="javascript:;" id="modConfirma" class="modConfirma modConfirm">confirma</a>
            	<a href="javascript:;" id="modCancela" class="modCancela modConfirm">cancela</a>
            </div><!-- /confirmar -->
            <div id="modAlert">
                <p class="nome modMensagem"><strong></strong></p>
                <a href="javascript:;" onclick="closeModal()" id="modOk" class="modOk" style="margin-top: 100px">ok</a>
            </div>
        </div><!--/alert-->
    </div><!-- /modal -->

	<div id="all">

    	<div id="logoPrincipal2">
            @if(empty(Session::get('dataEleicao')[0]->ele_logo))
                <img src="{{secure_asset('img/logo/logoBisa.png')}}" alt=""  />
            @else
                <img src="{{url(Session::get('dataEleicao')[0]->ele_logo)}}" width="180" alt=""  />
            @endif
        </div><!-- /logo -->

        <div id="content3" class="clear">

            <div id="bgTop">
            </div>

            <div id="bgRight">
            </div>

            <div id="bgLeft">
            </div>

            <div id="bgBottom">
            </div>
            <br /><br /><br /><br />
            
            <?php
                $nameRoute = Route::currentRouteName();

                $activeEleicao = $activeEleitor = $activeCargo = $activeCandidatos = $activeZona = $activeComissao = $activeResumo = $activeBu = '';
                switch($nameRoute){
                    case 'lista.eleicoes':
                        $activeEleicao = ' class="active"';
                        break;
                    case 'lista.eleitores.comisao':
                        $activeEleitor = ' class="active"';
                        break;
                    case 'add.eleitor.comissao':
                        $activeEleitor = ' class="active"';
                        break;
                    case 'lista.cargos.comissao':
                        $activeCargo = ' class="active"';
                        break;
                    case 'add.cargos.comissao':
                        $activeCargo = ' class="active"';
                        break;
                    case 'edit.cargos.comissao':
                        $activeCargo = ' class="active"';
                        break;
                    case 'lista.candidatos.comissao':
                        $activeCargo = ' class="active"';
                        $activeCandidatos = ' >> Lista de Candidatos';
                        break;
                    case 'add.candidato.comissao':
                        $activeCargo = ' class="active"';
                        $activeCandidatos = ' >> Adicionar de Candidato';
                        break;
                    case 'lista.zona.comissao':
                        $activeZona = ' class="active"';
                        break;
                    case 'add.zona.comissao':
                        $activeZona = ' class="active"';
                        break;
                    case 'lista.comissao.comissao':
                        $activeComissao = ' class="active"';
                        break;
                    case 'add.comissao.comissao':
                        $activeComissao = 'class="active"';
                        break;
                    case 'lista.eleitores.comissao':
                        $activeEleitor = ' class="active"';
                        break;
                    case 'add.eleitores.comissao':
                        $activeEleitor = 'class="active"';
                        break;
                    case 'resumo.comissao':
                        $activeResumo = ' class="active"';
                        break;
                    case 'apuracaofinal.comissao':
                        $activeBu = ' active';
                        break;
                    case 'resumo.apuracao.eleicao.comissao':
                        $activeBu = ' active';
                        break;
                    default;
                }
            ?>
            <ul class="nav nav-tabs" style="margin-left: 20px">
                <li role="presentation"<?php echo $activeCargo;?>><a href="{{route('lista.cargos.comissao')}}">Cargos{{$activeCandidatos}}</a></li>
                <li role="presentation"<?php echo $activeZona;?>><a href="{{route('lista.zonas.comissao')}}">Zona Eleitoral</a></li>
                <!-- <li role="presentation"<?php echo $activeComissao;?>><a href="{{route('lista.comissao.comissao')}}">Comissão Eleitoral</a></li> -->
                <li role="presentation"<?php echo $activeEleitor;?>><a href="{{route('lista.eleitores.comissao')}}">Eleitores</a></li>
                <!-- <li role="presentation"<?php //echo $activeResumo;?>><a href="{{route('resumo.comissao')}}">Resumo da Eleição</a></li> -->
                <li role="presentation" class="apuracaofinal<?php echo $activeBu;?>"><a href="{{route('apuracaofinal.comissao')}}">Relatórios da Eleição</a></li>
                <li role="presentation"><a href="{{ route('user.logout') }}">Sair</a></li>
            </ul>
