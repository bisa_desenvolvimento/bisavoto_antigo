@extends('email')
@section('content')

<div class="content-admin">
    <div id="logoEleicao">[logo]</div>
    <h3>[eleicao]</h3>
    Olá [nome],<br /><br />

    [corpo]
    <br />
    <br />

    Dúvidas: suporte@bisa.com.br

</div>
@endsection
