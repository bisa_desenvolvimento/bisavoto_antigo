@extends('admin')
@section('content')
<div class="content-admin">
  <h3>Cadastro de Eleição</h3>
  <form method="POST" action="{{ route(empty($ele_id) ?'salvar.eleicoes' : 'update.eleicoes') }}" accept-charset="UTF-8" enctype="multipart/form-data" onsubmit="return dateDiffEleicoes()">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <input name="ele_id" type="hidden" value="{{ $ele_id}}">
    <div class="form-group">
      <label for="ele_nome" >Nome do pleito</label>
      <div class="input-group">
        <input value="{{$ele_nome}}" required="required" type="text" class="form-control" name="ele_nome" id="ele_nome" placeholder="Informe o nome do pleito">
        <div class="input-group-addon">*</div>
      </div>
    </div>

   <div class="form-group">
      <label for="ele_descricao">Descrição do pleito</label>
      <div class="input-group">
        <textarea required="required" class="form-control" rows="5" id="ele_descricao" name="ele_descricao" placeholder="Informe a descrição do pleito">{{$ele_descricao}}</textarea>
        <div class="input-group-addon">*</div>
      </div>
   </div>

  <div class="form-group">
      <label>Data e horário da eleição</label>
      <br/>
      <div class="col-sm-5">
          <label for="ele_horaInicio">Início</label>
          <div class="input-group">
            <input value="{{$ele_horaInicio}}" type="text" id="ele_horaInicio" name="ele_horaInicio" class="form-control datepicker" required="required">
            <div class="input-group-addon">*</div>
          </div>
          <br/>
          <label for="ele_horaTermino">Fim</label>
          <div class="input-group">
            <input value="{{$ele_horaTermino}}" type="text" id="ele_horaTermino" name="ele_horaTermino" class="form-control datepicker" required="required">
            <div class="input-group-addon">*</div>
          </div>
      </div>
  </div>
  <br class="clear" />
  <div class="form-group ">
      <label for="ele_qtdVotosCandidatos">Quantidade de votos gerais</label>
      <br />
      <div class="input-group col-md-2">
          <input value="{{$ele_qtdVotosCandidatos}}" type="text" class="form-control numeric" name="ele_qtdVotosCandidatos" id="ele_qtdVotosCandidatos" required="true">
          <div class="input-group-addon">*</div>
      </div>
  </div>
  <div class="form-group">
      <label for="ele_tempo">Tempo da Sessão</label>
      <br />
      <div class="input-group col-md-3">
          <input value="{{$ele_tempo}}" type="text" class="form-control numeric" name="ele_tempo" id="ele_tempo" required="true">
          <div class="input-group-addon">*</div>
      </div>
  </div>

  <div class="form-group">
      <label for="ele_qtdVotosCandidatos">Quantidade de candidatos para cada voto</label>
      <div class="input-group col-md-2">
        <div class="input-group">
          <input value="{{$ele_qtdVotosCandidatos}}" type="text" class="form-control numeric" name="ele_qtdVotosCandidatos" id="ele_qtdVotosCandidatos" required="true">
          <div class="input-group-addon">*</div>
        </div>
      </div>
  </div>

  <div class="form-group">
      <label for="ele_categoriaEleitor">Categoria de eleitores  </label>
      <input value="{{$ele_categoriaEleitor}}" type="text" class="form-control" name="ele_categoriaEleitor" placeholder="Informe a Categoria de Eleitores" />
  </div>

  <div class="form-group">
    <label for="ele_qtdBu">Quantidade de BU’s por Urna</label>
      <br class="clear" />
      <div class="input-group col-md-2">
          <input value="{{$ele_qtdBu}}" type="text" class="form-control numeric" name="ele_qtdBu" placeholder="BU’s p/ Urna" required="true" />
          <div class="input-group-addon">*</div>
      </div>
  </div>
    
   <div>
      @if(Session::has('erro'))
        <p class="errors">{!! Session::get('erro') !!}</p>
      @endif
    </div>
    <div class="form-group">
      <label for="ele_logo">Logomarca da eleição</label>
      <div class="input-group">
        <p>Sugestão de dimensões para imagem: 650 X 380 pixels.</p>
        <input type="file" name="ele_logo" id="ele_logo" @if($ele_id == '') required="true" @endif;>
        @if($ele_logo != '')
          <br><img src="{{url($ele_logo) }}" />
        @endif
      </div>
    </div>

    <div class="form-group">
    <label for="ele_qtdSessoes">Quantidade de sessões ao final do pleito</label>

    <div class="input-group col-md-2">
      <input value="{{$ele_qtdSessoes}}" type="text" class="form-control numeric" name="ele_qtdSessoes" placeholder="Sessões" required="true" />
      <div class="input-group-addon">*</div>
    </div>
  </div>

  
  <div class="form-group">
    <label for="ele_qtdSessoes">Permitir votos Brancos?</label>

    <div class="input-group col-md-2">
      <input type="radio" id="exibir_branco_sim" name="exibir_branco" value="1">
      <label for="exibir_branco_sim">Sim</label><br>
    </div>
    <div class="input-group col-md-2">
      <input type="radio" id="exibir_branco_nao" name="exibir_branco" value="0" checked>
      <label for="exibir_branco_nao">Não</label><br>
    </div>
  </div>
  
  <div class="form-group">
    <label for="ele_qtdSessoes">Permitir votos Nulos?</label>

    <div class="input-group col-md-2">
      <input type="radio" id="exibir_nulo_sim" name="exibir_nulo" value="1">
      <label for="exibir_nulo_sim">Sim</label><br>
    </div>
    <div class="input-group col-md-2">
      <input type="radio" id="exibir_nulo_nao" name="exibir_nulo" value="0" checked>
      <label for="exibir_nulo_nao">Não</label><br>
    </div>

  </div>

  <br/>
  

  <div class="form-group">
    <label for="enviar_comprovante">Enviar comprovante de email de votação por email?</label>

      <div class="input-group col-md-2">
        <input type="radio" id="enviar_comprovante_sim" name="enviar_comprovante" value="1">
          <label for="enviar_comprovante_sim">Sim</label><br>
      </div>
      <div class="input-group col-md-2">
        <input type="radio" id="enviar_comprovante_nao" name="enviar_comprovante" value="0" checked>
          <label for="enviar_comprovante_nao">Não</label><br>
      </div>
  </div>

  <div class="form-group">
    <label for="enviar_comprovante">Exibir sequencia de votos?</label>

      <div class="input-group col-md-2">
        <input type="radio" id="exibir_sequenciaVotos" name="exibir_sequenciaVotos" value="1">
          <label for="exibir_sequenciaVotos_sim">Sim</label><br>
      </div>
      <div class="input-group col-md-2">
        <input type="radio" id="exibir_sequenciaVotos" name="exibir_sequenciaVotos" value="0" checked>
          <label for="exibir_sequenciaVotos_nao">Não</label><br>
      </div>
  </div>

  <div class="form-group">
    <label for="ele_tipo">Tipo de Votação</label>
    <select class="form-control" name="ele_tipo" id="ele_tipo">
      <option value="1">ELEIÇÃO</option>
      <option value="2">ASSEMBLEIA</option>
    </select>
  </div>

  <div class="form-group">
      <label for="enviar_comprovante">Habilitar Exclusão de Eleitor no processo de Eleição?</label>

      <div class="input-group col-md-2">
        <input type="radio" id="habilitar_exclusao" name="habilitar_exclusao" value="1">
          <label for="habilitar_exclusao">Sim</label><br>
      </div>
      <div class="input-group col-md-2">
        <input type="radio" id="habilitar_exclusao" name="habilitar_exclusao" value="0" checked>
          <label for="habilitar_exclusao">Não</label><br>
      </div>
  </div>

  <div class="form-group">
      <label for="aceitar_email_duplicado">Aceitar e-mails duplicados?</label>

      <div class="input-group col-md-2">
          <input type="radio" id="aceitar_email_duplicado" name="aceitar_email_duplicado" value="1">
          <label for="aceitar_email_duplicado">Sim</label><br>
      </div>
      <div class="input-group col-md-2">
        <input type="radio" id="aceitar_email_duplicado" name="aceitar_email_duplicado" value="0" checked>
          <label for="aceitar_email_duplicado">Não</label><br>
      </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-default">Enviar</button>
  </div>  
  </form>

</div>

@if ($exibir_branco)
      <script type="text/javascript">
        $('input:radio[name=exibir_branco][value="{{ $exibir_branco }}"]').prop("checked", true);
      </script>
@endif

@if ($exibir_nulo)
      <script type="text/javascript">
        $('input:radio[name=exibir_nulo][value="{{ $exibir_nulo }}"]').prop("checked", true);
      </script>
@endif

@if ($enviar_comprovante)
<script type="text/javascript">
  $('input:radio[name=enviar_comprovante][value="{{ $enviar_comprovante }}"]').prop("checked", true);
</script>
@endif

@if ($exibir_sequenciaVotos)
<script type="text/javascript">
  $('input:radio[name=exibir_sequenciaVotos][value="{{ $exibir_sequenciaVotos }}"]').prop("checked", true);
</script>
@endif

@if ($habilitar_exclusao)
<script type="text/javascript">
  $('input:radio[name=habilitar_exclusao][value="{{ $habilitar_exclusao }}"]').prop("checked", true);
</script>
@endif

@if ($aceitar_email_duplicado)
<script type="text/javascript">
  $('input:radio[name=aceitar_email_duplicado][value="{{ $aceitar_email_duplicado }}"]').prop("checked", true);
</script>
@endif

<script type="text/javascript">
  $(document).ready(function(){
    $("#ele_tipo").val("{{$ele_tipo}}");
  })
</script>
@endsection
