@extends('email')
@section('content')

<div class="content-admin">
    <div id="logoEleicao">[logo]</div>
    <h3>[eleicao]</h3>
    Olá [nome],<br /><br />
    Inicio da Eleição: [dataInicio]<br />
    Término da Eleição: [dataTermino]<br /><br />

    Acesse: <a href="{{ url('eleicao') }}/[alias]">{{ url('eleicao') }}/[alias]</a><br />
    Login: [login]<br />
    Senha: [senha]
    <br />
    <br />

    Dúvidas: suporte@bisa.com.br

</div>
@endsection
