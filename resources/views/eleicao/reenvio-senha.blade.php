@extends('email')
@section('content')
<style>
.page-break {
    page-break-after: always;
}
</style>
<div class="content-admin">
    [msg]<br/><br/>
    Eleição: [eleicao]<br/>
    Nome: [nome]<br/>
    E-mail: [email]<br/>
    Matricula: [matricula]<br/>
    CPF: [cpf]<br/>
    Telefone: [telefone]<br/>
</div>
<div class="page-break"></div>
@endsection
