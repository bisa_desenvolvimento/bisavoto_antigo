@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<div id="divImportarArquivo" class="fileupload" style="">

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)

            <script type="text/javascript">

                $(document).ready(function(){
                    modalErro('alert','{{ $error }}');
                });

            </script>
            @endforeach
        @endif

    <fieldset>
        <legend>Importar Eleitores</legend>
        <p class="text-primary">*Ordem dos cabeçalhos para importação: zona eleitoral | nome do eleitor | cpf | matricula | email </p>

         {{-- <form role="form" action="{{route('importar.arquivo.eleitores')}}" files="true"  method="POST" enctype="multipart/form-data"> --}}
         <form role="form" action="{{route('importar2.arquivo.eleitores')}}" files="true"  method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="profile_id" value="3">
            <input type="hidden" name="ele_id" value="{{$ele_id}}">

            <div class="form-group" style="">
                  <label for="arquivoCsv">Selecione o arquivo de importação</label>
                 <input type="file" required="required" name="arquivoCsv" id="arquivoCsv" accept=".csv" />
              <input type="submit" class="btn btn-primary" id="fazer_consulta">
            </div>

              
         </form>
   </fieldset>
</div>

<div id="modalErro" class="novoModal" style="display: none;">
    <div class="sombraErro"></div>
    <div id="alertErro">
        <div id="confirmarErro">
            <p class="nome" id="msg" style="font-size: 30px;margin-bottom: -68px;"></p>
            <img src="../../img/load.gif" id="loading" style="width: 50px;">
            <a href="{{ route('importar.eleitores', [$ele_id]) }}" id="btOK" class="modOkErro modAlertErro" style="display: none;">ok</a>
        </div><!-- /confirmar -->
    </div><!--/alert-->
</div>

<script>

    $('#fazer_consulta').click(function(){
        if ($('input[name=arquivoCsv]').val() != '') {
            $('.novoModal').css('display','block');
            $('#msg').text("Estamos importando o arquivo, aguarde.");
        }
    });

    function processarArquivo(processar = true){
        if(processar == true){
            $.ajax({
                url: "{{ route('importar2.arquivo.eleitores.processararquivojax') }}",
                dataType: 'json',
                async: false,
            })     
            .always(function(response) {
                $('.novoModal').css('display','block');
                $('#msg').text(response.mensagem);

                console.log(response.mensagem);
                if(response.status == false){
                    if(response.status == false){
                        if (response.erro) {
                            $('#loading').hide();
                            $('#btOK').show();
                        } else {
                            window.location ="{{ route('importar2.arquivo.eleitores.resultado') }}";   
                        }
                    }else{
                        $('#msg').text(response.mensagem);
                    }
                }else{                    
                    processarArquivo(response.status);
                }
            
            });
        }
    }

</script>


<?php 
        if(Session::get('leitura_sucesso')) { ?>
            <script>
                $('.novoModal').css('display','block');
                $('#msg').text("<?php echo Session::get('leitura_sucesso'); ?>");
                processarArquivo();
            </script>
<?php   
            Session::forget('leitura_sucesso');
        } ?>

@endsection
