<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use App\User as User;
class Voto extends Model {

    protected $table = 'voto';
    public $zona_id;
    public $id_ele;
    public $id;
    private $objVoto;
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = array('ele_id', 'cdt_id', 'car_id');

    protected $casts = ['zon_id' => 'int',
        'cdt_id' => 'string',
        'ele_id' => 'int',
        'car_id'=>'int'];

    protected $fillable = array(
        'vot_sequencial',
        'zon_id',
        'ele_id',
        'cdt_id',
        'car_id');

    public function __construct()
    {
        $this->zona_id = Auth::user()->zona_id;
        $this->id_ele = Auth::user()->ele_id;
        $this->id = Auth::user()->id;
    }

    public function addVoto($votos, $cargo)
    {
        try{
            DB::beginTransaction();
            for($x = 0; $x < count($votos); $x++){
                $objVoto = new Voto();
                $objVoto->ele_id = $this->id_ele;
                $objVoto->vot_sequencial = $this->maxVotoCargo(intval($votos[$x]), intval($cargo[$x]));
                $objVoto->zon_id = $this->zona_id;
                $objVoto->cdt_id = $votos[$x];
                $objVoto->car_id = intval($cargo[$x]);
                $objVoto->save();
            }
            $user = new User();
            $user = User::where('id', '=', $this->id)->firstOrFail();
            $user->vote = 1;
            $user->save();

            DB::commit();
            return true;
        } catch (Exception $e){
            DB::rollback();
            return false;
        }
    }

    public function maxVotoCargo($voto, $cargo)
    {
        $retorno = Voto::where('cdt_id', $voto)
                        ->where('car_id', $cargo)
                        ->max('vot_sequencial');
        return $retorno + 1;
    }

}
