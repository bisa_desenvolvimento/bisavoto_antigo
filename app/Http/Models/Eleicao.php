<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Eleicao extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'eleicao';
	protected $primaryKey = 'ele_id';
	protected $fillable = ['ele_nome',
						   "ele_descricao",
						   "ele_horaInicio",
						   "ele_horaTermino",
						   "ele_qtdVotosCandidatos",
						   "ele_tempo",
						   "ele_categoriaEleitor",
						   "ele_qtdBu",
						   "ele_qtdSessoes",
						   "ele_logo",
						   "alias",
						   "ele_diff",
                           "ele_tipo",
                           "exibir_branco",
                           "exibir_nulo",
                            "enviar_comprovante",
                            "exibir_sequenciaVotos",
                            "habilitar_exclusao",
                            "aceitar_email_duplicado"
                        ];

    public static function listar()
    {
        return DB::table("eleicao")
            ->selectRaw("
            eleicao.*,
            (SELECT count(lg.log_id) FROM `log` AS lg WHERE lg.log_idregistro = eleicao.ele_id AND lg.log_operacao = 'RESUMO_MODEL_RESUMO::ZERESIMA') 
            AS zeresima
            ")
            ->orderBy("eleicao.ele_nome")
            ->paginate(10);
            
    }

    public static function listarEleicoesFiltro($filtro)
    {
        $query = DB::table("eleicao")
        ->selectRaw("
            eleicao.*,
            (SELECT count(lg.log_id) FROM `log` AS lg WHERE lg.log_idregistro = eleicao.ele_id AND lg.log_operacao = 'RESUMO_MODEL_RESUMO::ZERESIMA') 
            AS zeresima
        ")
        ->orderBy("eleicao.ele_horaTermino");
    
        if ($filtro == 'realizadas') {
            $query->whereDate('eleicao.ele_horaTermino', '<=', date('Y-m-d'));
        } elseif ($filtro == 'nao-realizadas') {
            $query->whereDate('eleicao.ele_horaTermino', '>', date('Y-m-d'));
        }
    
        $result = $query->paginate(10);
        return $result;
    
    }

	 public function get($id)
    {
        return Eleicao::where('ele_id', $id)
                ->selectRaw("
                eleicao.*,
                (SELECT count(lg.log_id) FROM `log` AS lg WHERE lg.log_idregistro = eleicao.ele_id AND lg.log_operacao = 'RESUMO_MODEL_RESUMO::ZERESIMA') 
                AS zeresima
                ")
                ->get();
    }

	public function getById($id)
    {
        $list = Eleicao::where('ele_id', $id)->first();

        return $list;
    }

	public function getByAlias($alias)
    {
        $list = Eleicao::where('alias', $alias)->first();

        return $list;
    }

    public function addVoto($votos, $cargos, $ele_id, $zona_id)
    {
        try {
            DB::beginTransaction();
            foreach ($votos as $key => $voto):

                $arrInsert = [
                    'cdt_id' => $voto,
                    'car_id' => $cargos[$key],
                    'zon_id' => $ele_id,
                    'ele_id' => $zona_id
                ];
                Eleicao::create($arrInsert);
            endforeach;
            DB::commit();
            return 'ok';
        }catch (Exception $e){
            DB::rollback();
            return 'error';
        }
    }

    public function verificarOpcaoEmailDuplicado($ele_id){
        $result = DB::table('eleicao')->select('aceitar_email_duplicado')->where('ele_id', '=', $ele_id)->get();
        $result = $result[0];
        
        return $result->aceitar_email_duplicado;
    }

    public function verificaOpcaoComprovanteEmail($ele_id){
        $result = DB::table('eleicao')->select('enviar_comprovante')->where('ele_id', '=', $ele_id)->get();
        $result = $result[0];
        
        return $result->enviar_comprovante;
    }

    public function verificaOpcaoExibirSequenciaVotos($ele_id){
       
        $result = DB::table('eleicao')->select('exibir_sequenciaVotos')->where('ele_id', '=', $ele_id)->get();
        $result = $result[0];
        
        return $result->exibir_sequenciaVotos;
    }

    public function verificaOpcaoHabilitarExclusao($ele_id){
       
        $result = DB::table('eleicao')->select('habilitar_exclusao')->where('ele_id', '=', $ele_id)->get();
        $result = $result[0];
        
        return $result->habilitar_exclusao;
    }


    public function verificarEleicaoEncerrada($ele_id)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $retorno = false;

        $objEleicao = $this->get($ele_id);

        if($objEleicao)
        {
            $horaTermino = $objEleicao[0]->ele_horaTermino;
        }
        $horaTermino = strtotime($horaTermino);
        $horaAgora   = time();
        $difDias     =  floor(($horaTermino-$horaAgora)/3600/24);

        if($difDias <= 0)
        {
            $retorno = true;
        }
        return $retorno;
    }

    /** Mantis 16783 - Marcos Antonio */
    public function verificarEmissaoZeresima($ele_id){
        $log = DB::table('log')->where('log_idregistro', $ele_id)->where('log_operacao', 'RESUMO_MODEL_RESUMO::ZERESIMA')->first();

        if(isset($log)){
            return true;
        }

        return false;
    }
    /** FIM Mantis 16783 - Marcos Antonio */
}
