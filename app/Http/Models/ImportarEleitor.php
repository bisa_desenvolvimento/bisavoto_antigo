<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ImportarEleitor extends Model {

	protected $table = 'importacao_eleitores';
	protected $primaryKey = 'id';

	protected $fillable = array('idEleicao',
								'zona',		 
							    'nome',
                                'cpf',
                                'matricula',
                                'email',
								'verificado',
                                'importado',
								'duplicados',
                                'campo_obrigatorio',
								'email_invalido');

	public function salvarArquivo($dados) {
		ImportarEleitor::insert($dados);  
	}

	public function atualizarDado($id, $importado, $duplicado, $campo_obrigatorio, $email_invalido,  $cpf_invalido) {
		$sql = "UPDATE importacao_eleitores set verificado = 1, importado = {$importado}, duplicados = {$duplicado}, campo_obrigatorio = {$campo_obrigatorio}, email_invalido = {$email_invalido}, cpf_invalido = {$cpf_invalido} where id = {$id};";
        $retorno = DB::update($sql);
        return $retorno;
	}

	public function listarDados() {
		$sql = "SELECT * FROM importacao_eleitores where verificado <> 1 ORDER BY verificado LIMIT 100";
        $retorno = DB::select($sql);
        return $retorno;
	}

	public function totalDadosProcessados()	{
		$sql = "SELECT COUNT(id) AS total,
				(SELECT COUNT(id) FROM importacao_eleitores WHERE verificado <> 0) AS verificados
			FROM
				importacao_eleitores;";
		$retorno = DB::select($sql);
		return $retorno;
	}

	public function apagarDadosArquivo() {
		$sql = "DELETE FROM importacao_eleitores WHERE id > 0;";
		$retorno = DB::delete($sql);
		return $retorno;
	}

	public function listaResultado($tipo) {
		$sql = "";
		switch ($tipo) {
			case 'duplicado':
				$sql = "SELECT * FROM importacao_eleitores where duplicados = 1";
				break;
			case 'email_invalido':
				$sql = "SELECT * FROM importacao_eleitores where email_invalido = 1";
				break;
			case 'cpf_invalido':
				$sql = "SELECT * FROM importacao_eleitores where cpf_invalido = 1";
				break;
			case 'importado':
				$sql = "SELECT * FROM importacao_eleitores where importado = 1";
				break;
			case 'campo_obrigatorio':
				$sql = "SELECT * FROM importacao_eleitores where campo_obrigatorio = 1";
				break;
		}
		
        $retorno = DB::select($sql);
        return $retorno;
	}
	
	
}
