<?php

namespace App\Http\Controllers;
require __DIR__.'/../../Library/vendor/autoload.php';

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Http\Request;
use Input;
use App\Http\Models\Eleitor;
use App\Http\Models\Eleicao;
use App\Http\Models\Endereco;
use App\Http\Models\ImportarEleitor;
use Auth;
use App\Http\Models\Zonas;
use View;
use Hash;
use PDF;
//use App\Library\HZip AS HZip;
use ZipArchive;
use Response;
use App\Library\Functions as Functions;
use App\Http\Models\Log as Log;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class EleitorController extends Controller
{


	private $model;
	private $modelEndereco;
	private $profile_id;
	private $ele_id;
	private $content;
	private $body;
	private $bodyAll;
	private $etiqueta;
	private $path;
	private $header;
	private $footer;
	private $log;

	public function __construct()
	{
		if (is_null($this->model)) {
			$this->model = new Eleitor();
			$this->modelEndereco = new Endereco();
			$this->profile_id = Auth::user()->profile_id;
			$this->ele_id = Auth::user()->ele_id;
			$this->log = new Log();
			$this->id = Auth::user()->id;
		}

		$this->path = public_path();
	}

	public function importarEleitores($ele_id)
	{
		return view('eleitores.importar-eleitor', compact('ele_id'));
	}
	private function inserirEleitorImportacao($arrInserirEleitorImportacao)
	{
		return $this->model->inserirEleitorImportacao($arrInserirEleitorImportacao);
	}

	private function existeEleitorEleicaoZona(
		$idEleicao,
		$numZona,
		$matricula
	) {
		$retorno = $this->model->existeEleitorEleicaoZona(
			$idEleicao,
			$numZona,
			$matricula
		);
		return ($retorno) ? true : false;
	}
	private function verificarDuplicados($arr)
	{
		$arr_unique     = array_unique($arr);
		$arr_duplicates = array_diff_assoc($arr, $arr_unique);

		return array_values($arr_duplicates);
	}
	public function prepararInserirEleitorImportacao($idEleicao, $arrDadosCsv)
	{
		//dd($arrDadosCsv);
		/**
		 * array [0] - zona
		 * 		 [1] - nome
		 *	     [2] - cpf
		 *       [3] - matricula
		 *       [4] - email
		 *       [5] - endereco
		 *       [6] - cep
		 *       [7] - cidade
		 *       [8] - estado
		 */

		if (!$this->is_utf8($arrDadosCsv[1])) {
			$arrDadosCsv[1] = utf8_encode($arrDadosCsv[1]);
		}

		if (!$this->is_utf8($arrDadosCsv[2])) {
			$arrDadosCsv[2] = utf8_encode($arrDadosCsv[2]);
		}

		$arrDadosCsv[2] = preg_replace('/[^0-9]/', '', $arrDadosCsv[2]);
		$arrDadosCsv[2] = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "$1.$2.$3-$4", $arrDadosCsv[2]);


		return  [
			'ele_id' => $idEleicao,
			'name' => $arrDadosCsv[1],
			'cpf' => $arrDadosCsv[2],
			'matricula' => str_replace(' ', '', $arrDadosCsv[3]),
			'email' =>  str_replace(' ', '', $arrDadosCsv[4]),
			'zona_id' => str_replace(' ', '', $arrDadosCsv[0]),
			'profile_id' => 3,
			'updated_at' => date("Y-m-d H:i:s"),
			'created_at' => date("Y-m-d H:i:s")
		];
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function processarArquivoEleitores()
	{
		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);


		$arrInserirEleitorImportacao 	  = [];
		$arrEleitoresDuplicados           = [];
		$arrEleitoresJaCadastrados  	  = [];
		$arrEleitorCampoObrigatorioBranco = [];
		$arrEleitorCampoEmailInvalido     = [];

		$valoresForm = array_map('trim', Input::all());

		$idEleicao = $valoresForm['ele_id'];

		if (Input::hasFile('arquivoCsv')) {
			$arquivoCsv = Input::file('arquivoCsv');

			if (strtolower($arquivoCsv->getClientOriginalExtension()) != 'csv') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroExtensaoMessage(),
					]);
			}

			$csv    = array();
			$linhas = file($arquivoCsv, FILE_IGNORE_NEW_LINES);

			$linhas[0] = trim($linhas[0]);
			$cabecalho = explode(";", $linhas[0]);


			if (strtolower($cabecalho[0]) != 'zona') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[1]) != 'nome') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[2]) != 'cpf') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[3]) != 'matricula') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[4]) != 'email') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			}

			unset($linhas[0]); // remove os cabeçalhos

			$arrEleitoresDuplicados = $this->verificarDuplicados($linhas);

			foreach ($linhas as $linha) {
				$linha = trim($linha);
				$arrDadosCsv = explode(";", $linha);

				// verificando se existe alguma linha vazia
				if ($arrDadosCsv[0] == '' || $arrDadosCsv[1] == '' || $arrDadosCsv[2] == '' || $arrDadosCsv[3] == '' || $arrDadosCsv[4] == '') {
					return redirect($this->erroPath())
						->withErrors([
							'erro' => 'Existe eleitor com campo obrigatório vazio. Corrija o arquivo e tente novamente.',
						]);
				}

				$resultZona = $this->model->existeZona($idEleicao, str_replace(' ', '', $arrDadosCsv[0]));


				if ($resultZona == '0') {
					return redirect($this->erroPath())
						->withErrors([
							'erro' => $this->getErroZonaMessage(str_replace(' ', '', $arrDadosCsv[0])),
						]);
				} else {

					$numZonaresult = $this->model->pegarZonaId($idEleicao, str_replace(' ', '', $arrDadosCsv[0]));
					array_shift($arrDadosCsv);
					array_unshift($arrDadosCsv, "" . $numZonaresult . "");
				}


				if (!$this->is_utf8($arrDadosCsv[2])) {
					$arrDadosCsv[2] = utf8_encode($arrDadosCsv[2]);
				}

				$arrDadosCsv[2] = preg_replace('/[^0-9]/', '', $arrDadosCsv[2]);
				$arrDadosCsv[2] = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "$1.$2.$3-$4", $arrDadosCsv[2]);

				if ($this->validaCPF($arrDadosCsv[2]) == false) {

					if (!$this->is_utf8($arrDadosCsv[1])) {
						$arrDadosCsv[1] = utf8_encode($arrDadosCsv[1]);
					}

					return redirect($this->erroPath())
						->withErrors([
							'erro' => $this->getErroCPFMessage($arrDadosCsv[1]),
						]);
				}

				if (filter_var(str_replace(' ', '', $arrDadosCsv[4]), FILTER_VALIDATE_EMAIL) === false) {

					if (!$this->is_utf8($arrDadosCsv[1])) {
						$arrDadosCsv[1] = utf8_encode($arrDadosCsv[1]);
					}

					return redirect($this->erroPath())
						->withErrors([
							'erro' => $this->getErroEmailMessage($arrDadosCsv[1]),
						]);
				}


				/**
				 * array [0] - zona
				 * 		 [1] - nome
				 *	     [2] - cpf
				 *       [3] - matricula
				 *       [4] - email
				 *       [5] - endereco
				 *       [6] - cep
				 *       [7] - cidade
				 *       [8] - estado
				 */
				$numZona   = $numZonaresult;
				$nome      = $arrDadosCsv[1];
				$cpf       = $arrDadosCsv[2];
				$matricula = str_replace(' ', '', $arrDadosCsv[3]);
				$email     = str_replace(' ', '', $arrDadosCsv[4]);


				if (!$this->is_utf8($nome)) {
					$nome = utf8_encode($nome);
				}

				if ('' == trim($nome)  || '' == trim($matricula) || '' == trim($email) || '' == trim($numZona) || '' == trim($cpf)) {
					$arrEleitorCampoObrigatorioBranco[] = $arrDadosCsv;
				} else if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
					$arrEleitorCampoEmailInvalido[] = $arrDadosCsv;
				} else {
					$existeEleitorCadastrado = $this->existeEleitorEleicaoZona(
						$idEleicao,
						$numZona,
						$matricula
					);
					if ($existeEleitorCadastrado) {
						$arrEleitoresJaCadastrados[] = $arrDadosCsv;
					} else {
						$arrInserirEleitorImportacao[] = $this->prepararInserirEleitorImportacao($idEleicao, $arrDadosCsv);
					}
				}
			}

			if ($arrInserirEleitorImportacao) {
				$this->inserirEleitorImportacao($arrInserirEleitorImportacao);
			}
		}

		return view('eleitores.exibir-eleitores-posarquivo', compact(
			'arrEleitoresDuplicados',
			'arrEleitoresJaCadastrados',
			'arrEleitorCampoObrigatorioBranco',
			'arrEleitorCampoEmailInvalido'
		));
	}

	public function importarArquivo()
	{
		ob_start();
		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);

		$valoresForm = array_map('trim', Input::all());

		$idEleicao = $valoresForm['ele_id'];
	
		$eleicaoModel = new Eleicao();
		$eleicaoPermiteEmailDuplicado = $eleicaoModel->verificarOpcaoEmailDuplicado($idEleicao);
		$eleitoresComEmailsDuplicados = array();

		if (Input::hasFile('arquivoCsv')) {
			$arquivoCsv = Input::file('arquivoCsv');

			if (strtolower($arquivoCsv->getClientOriginalExtension()) != 'csv') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroExtensaoMessage(),
					]);
			}

			$csv    = array();
			$linhas = file($arquivoCsv, FILE_IGNORE_NEW_LINES);

			$linhas[0] = trim($linhas[0]);
			$cabecalho = explode(";", $linhas[0]);


			if (strtolower($cabecalho[0]) != 'zona') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[1]) != 'nome') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[2]) != 'cpf') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[3]) != 'matricula') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			} elseif (strtolower($cabecalho[4]) != 'email') {
				return redirect($this->erroPath())
					->withErrors([
						'erro' => $this->getErroCabecalhoMessage(),
					]);
			}

			unset($linhas[0]); // remove os cabeçalhos

			$importarEleitor = new ImportarEleitor();
			$importarEleitor->apagarDadosArquivo();

			foreach ($linhas as $linha) {

				$linha = trim($linha); 
				$arrDadosCsv = explode(";", $linha);

				// verificando se existe alguma linha vazia
				if ($arrDadosCsv[0] == '' || $arrDadosCsv[1] == '' || $arrDadosCsv[2] == '' || $arrDadosCsv[3] == '' || $arrDadosCsv[4] == '') {
					return redirect($this->erroPath())
						->withErrors([
							'erro' => 'Existe eleitor com campo obrigatório vazio. Corrija o arquivo e tente novamente.',
						]);
				}

				
				if(!$this->is_utf8($arrDadosCsv[1]))
				{
					$arrDadosCsv[1] = utf8_encode($arrDadosCsv[1]);
				}			


				// if(!$this->is_utf8($arrDadosCsv[2]))
				// {
				//     $arrDadosCsv[2] = utf8_encode($arrDadosCsv[2]);
				// }

				// $arrDadosCsv[2] = preg_replace('/[^0-9]/', '', $arrDadosCsv[2]);
				// $arrDadosCsv[2] = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "$1.$2.$3-$4", $arrDadosCsv[2]);

				$dados = [
					'idEleicao' => $idEleicao,
					'zona' => $arrDadosCsv[0],
					'nome' => $arrDadosCsv[1],
					'cpf' => $arrDadosCsv[2],
					'matricula' => $arrDadosCsv[3],
					'email' => $arrDadosCsv[4]
				];

				$emailDuplicado = false;
				if(!$eleicaoPermiteEmailDuplicado && $dados["email"] != "sememail@sememail.com.br"){
					//O código abaixo verifica se o email se repete no arquivo.
					$emailAtual = $dados["email"];
					$contadorEmailDuplicado = 0;

					foreach ($linhas as $eleitor) {
						$emailVerificacao = explode(";", $eleitor)[4];

						if($emailVerificacao == $emailAtual){
							$contadorEmailDuplicado++;
							if($contadorEmailDuplicado > 1){
								$eleitoresComEmailsDuplicados[] = $dados;
								$emailDuplicado = true;
								break;
							}
						}
					}
					
					//O código abaixo verifica se o e-mail já existe no banco de dados, caso já não tenha sido encontrada a inconsistência no arquivo.
					if(!$emailDuplicado){
						if(isset($this->model->getByEmailAndEleId($emailAtual,  $idEleicao)->email)){
							$eleitoresComEmailsDuplicados[] = $dados;
							$emailDuplicado = true;
						}
					}
				}
				
				if(!$emailDuplicado){
					$importarEleitor->salvarArquivo($dados);
				}
			}

			Session::put('eleitores_com_emails_duplicados', $eleitoresComEmailsDuplicados);

			Session::put('leitura_sucesso', 'Arquivo salvo com sucesso, aguarde enquanto estamos processando a leitura dos dados.');
			// $_SESSION['leitura_sucesso'] = 'Arquivo salvo com sucesso, aguarde enquanto estamos processando a leitura dos dados.'; 
			ob_end_clean();
			return redirect()->route('importar.eleitores', $idEleicao);
			// return view('eleitores.importar-eleitor', compact('idEleicao'));
		}
	}

	public function processararquivojax()
	{

		$importarEleitor = new ImportarEleitor();

		$lista = $importarEleitor->listarDados();

		foreach ($lista as $eleitor_dado) {
			$resultZona = $this->model->existeZona($eleitor_dado->idEleicao, str_replace(' ', '', $eleitor_dado->zona));

			if ($resultZona == '0') {
				echo json_encode(['status' => false, 'mensagem' => $this->getErroZonaMessage(str_replace(' ', '', $eleitor_dado->zona)), 'erro' => true]);
				exit;
			} else {
				$numZonaresult = $this->model->pegarZonaId($eleitor_dado->idEleicao, str_replace(' ', '', $eleitor_dado->zona));
			}
			$eleitor_dado->cpf = preg_replace('/[^0-9]/', '', $eleitor_dado->cpf);
			$eleitor_dado->cpf = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "$1.$2.$3-$4", $eleitor_dado->cpf);

			if ($this->validaCPF($eleitor_dado->cpf) == false) {
				// cpf inválido
				$importarEleitor->atualizarDado($eleitor_dado->id, 0, 0, 0, 0, 1);
			}

			/**
			 * array [0] - zona
			 * 		 [1] - nome
			 *	     [2] - cpf
			 *       [3] - matricula
			 *       [4] - email
			 *       [5] - endereco
			 *       [6] - cep
			 *       [7] - cidade
			 *       [8] - estado
			 */
			$numZona   = $numZonaresult;
			$nome      = $eleitor_dado->nome;
			$cpf       = $eleitor_dado->cpf;
			$matricula = str_replace(' ', '', $eleitor_dado->matricula);
			$email     = str_replace(' ', '', $eleitor_dado->email);


			if (!$this->is_utf8($nome)) {
				$nome = utf8_encode($nome);
			}

			if ('' == trim($nome)  || '' == trim($matricula) || '' == trim($email) || '' == trim($numZona) || '' == trim($cpf)) {
				// campo obrigatório
				$importarEleitor->atualizarDado($eleitor_dado->id, 0, 0, 1, 0, 0);
			} else if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				// email inválido
				$importarEleitor->atualizarDado($eleitor_dado->id, 0, 0, 0, 1, 0);
			} else {
				$existeEleitorCadastrado = $this->existeEleitorEleicaoZona($eleitor_dado->idEleicao, $numZona, $matricula);
				
				if ($existeEleitorCadastrado) {
					// registro duplicado
					$importarEleitor->atualizarDado($eleitor_dado->id, 0, 1, 0, 0, 0);
				} else {
					$eleitor = [
						'ele_id' => $eleitor_dado->idEleicao,
						'name' => $eleitor_dado->nome,
						'cpf' => 	$eleitor_dado->cpf,
						'matricula' => str_replace(' ', '', $eleitor_dado->matricula),
						'email' =>  str_replace(' ', '', $eleitor_dado->email),
						'zona_id' => str_replace(' ', '', $numZona),
						'profile_id' => 3,
						'updated_at' => date("Y-m-d H:i:s"),
						'created_at' => date("Y-m-d H:i:s")
					];
					$this->inserirEleitorImportacao($eleitor);
					$importarEleitor->atualizarDado($eleitor_dado->id, 1, 0, 0, 0, 0);
				}
			}
		}

		$resultado = $importarEleitor->totalDadosProcessados();

		if ($resultado[0]->total <> $resultado[0]->verificados) {
			echo json_encode(['status' => true, 'mensagem' => 'Foram verificados ' . $resultado[0]->verificados . ' registros de ' . $resultado[0]->total]);
		} else {
			echo json_encode(['status' => false, 'mensagem' => 'Arquivo processado!', 'inconsistencias' => false]);
		}
	}

	public function resultadoImportacao()
	{
		$importarEleitor = new ImportarEleitor();

		$eleitores_campo_obrigatorio = $importarEleitor->listaResultado('campo_obrigatorio');
		$eleitores_duplicado = $importarEleitor->listaResultado('duplicado');
		$eleitores_cpf_invalido = $importarEleitor->listaResultado('cpf_invalido');
		$eleitores_email_invalido = $importarEleitor->listaResultado('email_invalido');
		$eleitores_importados = $importarEleitor->listaResultado('importado');

		return view('eleitores.exibir-eleitores-posarquivo-2', compact(
			'eleitores_campo_obrigatorio',
			'eleitores_duplicado',
			'eleitores_cpf_invalido',
			'eleitores_email_invalido',
			'eleitores_importados'
		));
	}

	public function exportarEleitores(Request $request)
	{
		$ele_id=null;

		if ($this->profile_id == 2) {
			$ele_id = $this->ele_id;
		} else{
			$ele_id = $request->ele_id;
		}

		$ele_id = intval($ele_id);

		$situacaoVotoEleitor = 2; //ambos os eleitores
		switch ($request->situacaoVotoEleitor){
			case "nao": $situacaoVotoEleitor = 0; break; //apenas os que não votaram
			case "sim": $situacaoVotoEleitor = 1; break; //apenas os que votaram
		}
		$listaEleitores=null;
		if($situacaoVotoEleitor == 2){
			$listaEleitores = Eleitor::orderBy('name')
			->where('profile_id', 3)
			->where('ele_id', $ele_id)->get();
		}else{
			$listaEleitores = Eleitor::orderBy('name')
			->where('profile_id', 3)
			->where('ele_id', $ele_id)
			->where('vote', $situacaoVotoEleitor)->get();
		}

		//die(var_dump($listaEleitores ));
		$nomeArquivo = "eleitores.xlsx";
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Nome');
		$sheet->setCellValue('B1', 'Matrícula');
		$sheet->setCellValue('C1', 'Votou?');
		$linha=2;
		foreach ($listaEleitores as $eleitor) {
			$sheet->setCellValue('A'.$linha,  $eleitor->name);
			$sheet->setCellValue('B'.$linha, $eleitor->matricula);
			$sheet->setCellValue('C'.$linha, $eleitor->vote == 0?"Não":"Sim");
			$linha++;
		}
	
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: text/xlsx");
        header("Content-Disposition: attachment; filename=$nomeArquivo");
		$writer->save("php://output");
	}

	public function index($ele_id = null)
	{
		$profile_id = $this->profile_id;
		if ($this->profile_id == 2) {
			$ele_id = $this->ele_id;
			
			/** Mantis 16783 - Marcos Antonio */
			$eleicaoModel = new Eleicao();
			$eleicao = $eleicaoModel->getById($ele_id);
			$emissaoZeresima = $eleicao->verificarEmissaoZeresima($eleicao->ele_id);
			
			$dataHoje = strtotime(date('Y-m-d H:i:s')); 
			$dataEleicao = strtotime($eleicao->ele_horaInicio);
			$diferencaEntreDatasEmSegundos = $dataEleicao - $dataHoje;
			if($diferencaEntreDatasEmSegundos > 0){
				$tempoParaIniciarAEleicaoEmMinutos = round(($diferencaEntreDatasEmSegundos/60), 2);
			}else{
				$tempoParaIniciarAEleicaoEmMinutos = 0;
			}
			/** FIM Mantis 16783 - Marcos Antonio */
		} else {
			Session::put('ele_id', $ele_id);
		}
		$ele_id = intval($ele_id);


		if (isset($_GET['busca'])) {
			$params = $_GET['busca'];
			$listaEleitores = Eleitor::orderBy('name')
				->where('profile_id', 3)
				->where('ele_id', $ele_id)
				->where(function ($query) use ($params) {
					$query->where('name', 'like', '%' . $params . '%')
						->orWhere('name', 'like', '%' . $params . '%');
				});
		} else {
			$listaEleitores = Eleitor::orderBy('name')
				->where('profile_id', 3)
				->where('ele_id', $ele_id);
		}



		$listaEleitores = $listaEleitores->paginate(50);
		$listaEleitores->setPath("");

		return view('eleitores.lista-eleitores', compact('listaEleitores', 'emissaoZeresima', 'tempoParaIniciarAEleicaoEmMinutos', 'ele_id'));
	}

	public function consultarZonasEleitorais($ele_id)
	{
		return Zonas::orderBy('zon_nome')->where('ele_id', $ele_id)->get();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function add($ele_id = null)
	{
		if ($this->profile_id == 2) {
			$ele_id = $this->ele_id;
		}
		$listaZonas = $this->consultarZonasEleitorais($ele_id);

		$id = $name = $email = $matricula = $cpf = $zona_id = $cep = $logradouro = $cidade = $uf = '';



		return view('eleitores.add-eleitor', compact(
			'id',
			'listaZonas',
			'ele_id',
			'name',
			'email',
			'matricula',
			'cpf',
			'zona_id',
			'cep',
			'logradouro',
			'cidade',
			'uf'
		));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valoresForm = array_map('trim', Input::all());

		$eleicaoModel = new Eleicao();
		$eleicaoPermiteEmailDuplicado = $eleicaoModel->verificarOpcaoEmailDuplicado($valoresForm['ele_id']);

		if(!$eleicaoPermiteEmailDuplicado && $valoresForm['email']!="sememail@sememail.com.br"){
			if(isset($this->model->getByEmailAndEleId($valoresForm['email'], $valoresForm['ele_id'])->email)){
				return redirect($this->erroPath())
					->withErrors([
						'erro' => "E-mail já cadastrado. Use um e-mail diferente.",
					]);
			}
			
		}

		$objEleitor 			  = new Eleitor();
		$objEleitor->ele_id 	  = $valoresForm['ele_id'];
		$objEleitor->name   	  = $valoresForm['name'];
		$objEleitor->matricula    = $valoresForm['matricula'];
		$objEleitor->email  	  = $valoresForm['email'];
		$objEleitor->cpf  	  	  = $valoresForm['cpf'];
		$objEleitor->profile_id   = $valoresForm['profile_id'];
		$objEleitor->zona_id      = $valoresForm['zona_id'];

		$objEleitor->save();

		$idEleitor = $objEleitor->id;


		if ($idEleitor) {
			$objEndereco = new Endereco();
			$objEndereco->users_id = $idEleitor;
			$objEndereco->logradouro = $valoresForm['logradouro'];
			$objEndereco->cidade = $valoresForm['cidade'];
			$objEndereco->uf = $valoresForm['uf'];
			$objEndereco->cep = $valoresForm['cep'];

			$objEndereco->save();

			/** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
			$idUsuario = $this->id;
			$idRegistro = $objEleitor->ele_id;
			$this->log->salvar('ELEITOR_MODEL_ELEITOR::INCLUSÃO DE ELEITOR', $idRegistro, $idUsuario, array('Nome do Eleitor' => $objEleitor->name));
			/** FIM MANTIS 16869 **/
		}



		return redirect()->route('lista.eleitores', [$valoresForm['ele_id']]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($idEleicao, $idEleitor)
	{
		$eleitor = $this->model->get($idEleicao, $idEleitor);



		if ($eleitor) {
			$id         = $eleitor[0]->id;
			$ele_id     = $eleitor[0]->ele_id;
			$name       = $eleitor[0]->name;
			$email      = $eleitor[0]->email;
			$matricula  = $eleitor[0]->matricula;
			$cpf        = $eleitor[0]->cpf;
			$zona_id    = $eleitor[0]->zona_id;

			$endereco = $this->modelEndereco->get($eleitor[0]->id);
			$cep        = '';
			$logradouro = '';
			$cidade     = '';
			$uf         = '';
			if (count($endereco)) {
				$cep        = $endereco[0]->cep;
				$logradouro = $endereco[0]->logradouro;
				$cidade     = $endereco[0]->cidade;
				$uf         = $endereco[0]->uf;
			}
		}
		$listaZonas = $this->consultarZonasEleitorais($ele_id);

		return view('eleitores.add-eleitor', compact(
			'id',
			'ele_id',
			'name',
			'email',
			'matricula',
			'cpf',
			'zona_id',
			'cep',
			'logradouro',
			'cidade',
			'uf',
			'listaZonas'
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$valoresForm = array_map('trim', Input::all());
		$idRegistro = $valoresForm["ele_id"];
        $idUsuario = $this->id;

		$eleitor = $this->model->get(
			$valoresForm["ele_id"],
			$valoresForm["id"]
		);

		$emailAntigo = $eleitor[0]->email;
		$emailNovo = $valoresForm["email"];
		$operacao = 'RESUMO_MODEL_RESUMO::ALTERAÇÂO DE EMAIL'; 
        $dados = array('Nome' => $eleitor[0]->name, 'Email Antigo' => $emailAntigo, 'Email Novo' => $emailNovo);

		if($emailAntigo != $emailNovo){
			$this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

			$eleicaoModel = new Eleicao();
			$eleicaoPermiteEmailDuplicado = $eleicaoModel->verificarOpcaoEmailDuplicado($valoresForm['ele_id']);
	
			if(!$eleicaoPermiteEmailDuplicado && $emailNovo!="sememail@sememail.com.br"){
				if(isset($this->model->getByEmailAndEleId($emailNovo, $valoresForm['ele_id'])->email)){
					return redirect($this->erroPath())
						->withErrors([
							'erro' => "E-mail já cadastrado. Use um e-mail diferente.",
						]);
				}
				
			}
		} else {
			$emailAntigo = [];
			$emailNovo = [];
		}

		if ($eleitor[0]->id) {
			/** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
			if($eleitor[0]->name != $valoresForm["name"]){
				$this->log->salvar('ELEITOR_MODEL_ELEITOR::ALTERAÇÃO DE NOME DE ELEITOR', 
									$idRegistro, 
									$idUsuario, 
									array('Nome Antigo' => $eleitor[0]->name, 'Nome Novo' => $valoresForm["name"]));
			}

			if($eleitor[0]->matricula != $valoresForm["matricula"]){
				$this->log->salvar('ELEITOR_MODEL_ELEITOR::ALTERAÇÃO DE MATRÍCULA DE ELEITOR', 
									$idRegistro, 
									$idUsuario, 
									array('Nome do Eleitor'=>$valoresForm["name"],'Matrícula Antiga' => $eleitor[0]->matricula, 'Matrícula Nova' => $valoresForm["matricula"]));
			}

			if($eleitor[0]->cpf != $valoresForm["cpf"]){
				$this->log->salvar('ELEITOR_MODEL_ELEITOR::ALTERAÇÃO DE CPF DE ELEITOR', 
									$idRegistro, 
									$idUsuario, 
									array('Nome do Eleitor'=>$valoresForm["name"],'CPF Antigo' => $eleitor[0]->cpf , 'CPF Novo' => $valoresForm["cpf"]));
			}

			if($eleitor[0]->zona_id != $valoresForm["zona_id"]){
				$zonaAntiga = Zonas::where("zon_id", $eleitor[0]->zona_id)->get()->first()->zon_nome;
				$zonaNova = Zonas::where("zon_id", $valoresForm["zona_id"])->get()->first()->zon_nome;

				$this->log->salvar('ELEITOR_MODEL_ELEITOR::ALTERAÇÃO DE ZONA ELEITORAL DE ELEITOR', 
									$idRegistro, 
									$idUsuario, 
									array('Nome do Eleitor'=>$valoresForm["name"], 'Zona Eleitoral Antiga' => $zonaAntiga , 'Zona Eleitoral Nova' => $zonaNova ));
			}
			/** FIM MANTIS 16869 **/

			$eleitor[0]->ele_id = $valoresForm["ele_id"];
			$eleitor[0]->name   = $valoresForm["name"];
			$eleitor[0]->email  = $valoresForm["email"];
			$eleitor[0]->matricula = $valoresForm["matricula"];
			$eleitor[0]->cpf = $valoresForm["cpf"];
			$eleitor[0]->zona_id = $valoresForm["zona_id"];

			$eleitor[0]->save();


			$endereco = $this->modelEndereco->get($eleitor[0]->id);

			if (count($endereco)) {

				$endereco[0]->cep =  $valoresForm["cep"];
				$endereco[0]->logradouro =  $valoresForm["logradouro"];
				$endereco[0]->cidade = $valoresForm["cidade"];
				$endereco[0]->uf = $valoresForm["uf"];

				$endereco[0]->save();
			} else {

				$dadosEndereco = array(

					'users_id' => $eleitor[0]->id,
					'cep' => $valoresForm["cep"],
					'logradouro' => $valoresForm["logradouro"],
					'cidade' => $valoresForm["cidade"],
					'uf' => $valoresForm["uf"],
					'updated_at' => date("Y-m-d H:i:s"),
					'created_at' => date("Y-m-d H:i:s")

				);

				$this->modelEndereco->inserirEleitorEndereco($dadosEndereco);
			}

			return redirect()->route('lista.eleitores', [$valoresForm['ele_id']]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($ele_id, $idEleitor)
	{
		if (is_null($ele_id)) {
			$ele_id = $this->ele_id;
		}

		$eleitor = Eleitor::where('ele_id', $ele_id)
			->findOrFail($idEleitor);

		if ($eleitor->delete()) {

			/** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
			$idUsuario = $this->id;
			$idRegistro = $ele_id;
			$this->log->salvar('ELEITOR_MODEL_ELEITOR::EXCLUSÃO DE ELEITOR', $idRegistro, $idUsuario, array('Nome do Eleitor' => $eleitor->name));
			/** FIM MANTIS 16869 **/

			if ($this->profile_id == 2) {
				return redirect()->route('lista.eleitores');
			}

			return redirect()->route(
				'lista.eleitores',
				$ele_id
			);
		}
	}

	public function sendMail()
	{
		if ($this->profile_id == 2) {
			$ele_id = $this->ele_id;
		}
		$listaEleitores = Eleitor::orderBy('name')
			->where('ele_id', $ele_id)
			->where('profile_id', 3)
			->get();
		if ($listaEleitores) {
			foreach ($listaEleitores as $eleitor) {
			}
		}
	}

	public function gerarCarta($letra, $ele_id = null)
	{
		ini_set("auto_detect_line_endings", true);
		if ($this->profile_id == 2) {
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)) {
			$model_eleicao = new Eleicao();;
			$dadosEleicao = $model_eleicao->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}

		$arrLetra = Functions::alfabeto($letra);
		if ($letra == $arrLetra['total']) {
			$zip = new ZipArchive;
			$zipFileName = 'lista-eleitores.zip';
			$zip->open(public_path() . '/pdf/' . $ele_id . '/' . $zipFileName, ZipArchive::CREATE);
			foreach (glob(public_path() . '/pdf/' . $ele_id . '/*') as $fileName) {
				if (!strpos($fileName, '.zip')) {
					$file = basename($fileName);
					$zip->addFile($fileName, $file);
				}
			}
			$zip->close();

			$headers = array(
				'Content-Type' => 'application/octet-stream',
			);
			return  url() . '/pdf/' . $ele_id . '/' . $zipFileName;
		}
		$listaEleitores = $this->model->eleitorPorLetra($arrLetra['letra'], $ele_id);
		$letra++;
		$this->bodyAll = '';
		if (count($listaEleitores) > 0) {
			$this->content = View::make('eleicao.basecarta')->render();
			foreach ($listaEleitores as $eleitor) {
				$this->body = $this->content;
				$this->body = View::make('eleicao.basecarta')->render();
				$logo = $dadosEleicao->ele_logo;
				if (!file_exists($logo)) {
					$logo = '';
				} else {
					$logo = '<img src="' . $dadosEleicao->ele_logo . '" width="200" />';
				}

				$objEleitor = new Eleitor();
				$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $ele_id . $eleitor->id), 0, 6);
				$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);
				$this->body = str_replace('[logo]', $logo, $this->body);
				$this->body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $this->body);
				$this->body = str_replace('[nome]', $eleitor->name, $this->body);
				$this->body = str_replace('[dataInicio]', Functions::convertDateTime($dadosEleicao->ele_horaInicio), $this->body);
				$this->body = str_replace('[dataTermino]', Functions::convertDateTime($dadosEleicao->ele_horaTermino), $this->body);
				$this->body = str_replace('[login]', $eleitor->matricula, $this->body);
				$this->body = str_replace('[senha]', $newPass, $this->body);
				$this->bodyAll .= $this->body;
				flush();
			}

			if (!is_dir($this->path . '/pdf/' . $ele_id . '/')) {
				mkdir($this->path . '/pdf/' . $ele_id . '/', 0777, true);
				chmod($this->path . '/pdf/' . $ele_id . '/', 0777);
			}
			PDF::loadHTML($this->bodyAll)->setPaper('a4')->setOrientation('landscape')->setWarnings(false)->save($this->path . '/pdf/' . $ele_id . '/eleitores_letra_' . strtoupper($arrLetra['letra']) . '.pdf');
		}

		if ($this->profile_id == 2) {
			echo url("carta-eleitores/letra/$letra");
		} else {
			echo url("carta-eleitores/letra/$letra/idEleicao/$ele_id");
		}
	}


	public static function zipDir($sourcePath, $outZipPath)
	{
		$pathInfo = pathInfo($sourcePath);
		$parentPath = $pathInfo['dirname'];
		$dirName = $pathInfo['basename'];

		$z = new ZipArchive();
		$z->open($outZipPath, ZIPARCHIVE::CREATE);
		$z->addEmptyDir($dirName);
		self::folderToZip($sourcePath, $z, strlen("$parentPath/"));
		$z->close();
	}

	protected function getErroExtensaoMessage()
	{
		return 'Extensão Inválida';
	}

	protected function getErroCabecalhoMessage()
	{
		return 'Cabeçalho Inválido';
	}

	protected function getErroZonaMessage($zona)
	{
		return 'Zona "' . $zona . '" Não Existe No Sistema';
	}

	protected function getErroCPFMessage($eleitor)
	{
		return 'O Eleitor "' . $eleitor . '" Possui CPF Inválido';
	}

	protected function getErroEmailMessage($eleitor)
	{
		return 'O Eleitor "' . $eleitor . '" Possui Email Inválido';
	}

	public function erroPath()
	{
		return $_SERVER['HTTP_REFERER'];
	}

	public function is_utf8($str)
	{
		$c = 0;
		$b = 0;
		$bits = 0;
		$len = strlen($str);
		for ($i = 0; $i < $len; $i++) {
			$c = ord($str[$i]);
			if ($c > 128) {
				if (($c >= 254)) return false;
				elseif ($c >= 252) $bits = 6;
				elseif ($c >= 248) $bits = 5;
				elseif ($c >= 240) $bits = 4;
				elseif ($c >= 224) $bits = 3;
				elseif ($c >= 192) $bits = 2;
				else return false;
				if (($i + $bits) > $len) return false;
				while ($bits > 1) {
					$i++;
					$b = ord($str[$i]);
					if ($b < 128 || $b > 191) return false;
					$bits--;
				}
			}
		}
		return true;
	}


	function validaCPF($cpf = null)
	{

		// Verifica se um número foi informado
		if (empty($cpf)) {
			return false;
		}

		// Extrai somente os números
		$cpf = preg_replace('/[^0-9]/is', '', $cpf);

		// Verifica se foi informado todos os digitos corretamente
		if (strlen($cpf) != 11) {
			return false;
		}
		// Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
		if (preg_match('/(\d)\1{10}/', $cpf)) {
			return false;
		}
		// Faz o calculo para validar o CPF
		// for ($t = 9; $t < 11; $t++) {
		//     for ($d = 0, $c = 0; $c < $t; $c++) {
		//         $d += $cpf{$c} * (($t + 1) - $c);
		//     }
		//     $d = ((10 * $d) % 11) % 10;
		//     if ($cpf{$c} != $d) {

		//         return false;
		//     }
		// }

		return true;
	}
}
