<?php namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\RedirectResponse;
use Request;
use Route;
use Session;
use Redirect;
use Config;
use App\Http\Models\Eleicao as Eleicao;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

    public $eleicao;
    public function __construct()
	{
		$this->middleware('auth');
    }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $user = Auth::user();
        Session::put('dataUser', $user);

        switch($user->profile_id):
            case 1;
                return redirect('lista-eleicoes');
                break;
            case 2:
                $objEleicao = new Eleicao();
                $this->eleicao = $objEleicao->get($user->ele_id);
                Session::put('dataEleicao', $this->eleicao);
                return Redirect::route('lista.eleitores.comissao');
                break;
            case 3;
                if($user->vote == 1){
                    return $this->redirectToHome('Usuário já votou');
                }

                $objEleicao = new Eleicao();
                $this->eleicao = $objEleicao->get($user->ele_id);
                $hoje = date("Y-m-d H:i:d");
                $inicio = $this->eleicao[0]->ele_horaInicio;
                $fim = $this->eleicao[0]->ele_horaTermino;

                /** Mantis 16783 - Marcos Antonio */
			    $emissaoZeresima = $objEleicao->verificarEmissaoZeresima($user->ele_id);
                /** FIM Mantis 16783 - Marcos Antonio */

                if($inicio < $hoje && $hoje < $fim){
                    if(!$emissaoZeresima){
                        return $this->redirectToHome('Prezado Usuário, Estamos aguardando a emissão da ZERÉSIMA pela Comissão Eleitoral para poder dar início ao processo de votação. Dúvidas, entrar em contato com a mesma.');
                    }
                } else {
                    if($hoje > $fim){
                        return $this->redirectToHome('Votação encerrada para o dia de hoje');
                    }
                    return $this->redirectToHome('Aguarde o início da votação que será às '.$this->convertDateTime($inicio));
                }
                Session::put('dataEleicao', $this->eleicao[0]);
                return Redirect::route('lista.candidatos.eleitor');
                break;
            default;
                return redirect('user/login');
        endswitch;
	}

    public function redirectToHome($msg)
    {
        Session::flush();
        return redirect($_SERVER['HTTP_REFERER'])
            ->withInput(Request::only('cpf', 'remember'))
            ->withErrors([
                'cpf' => $msg,
            ]);
    }

    public function convertDateTime($date)
    {
        if (trim($date) != ''):
            if (strstr($date, '-')): // Formato x-x-x
                $date = explode(" ", $date);
                $hora = $date[1];
                $date = explode('-', $date[0]);
                $date = $date[2].'/'.$date[1].'/'.$date[0].' '.$hora;
            else:
                $date = explode(" ", $date);
                $hora = $date[1];
                $date = explode('/', $date[1]);
                $date = $date[2].'-'.$date[1].'-'.$date[0].' '.$hora;
            endif;

        endif;

        return $date;

    }

}
