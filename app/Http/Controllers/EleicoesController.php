<?php namespace App\Http\Controllers;

use App\Http\Models\Eleitor;
use Input;
use Redirect;
use Session;
use File;
use App\Http\Models\Eleicao as Eleicao;
use App\Zona;
use App\Cargo;
use App\Http\Models\Voto as Voto;
use App\Http\Models\Cargo as CargoModel;
use Auth;
use App\Http\Models\Eleitor as Eleitores;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\SendMailController as SendMailController;
use App\Http\Models\Log as Log;
use Hash;
use Carbon\Carbon;
use App\Library\Functions as Functions;
use Carbon\Carbon as CarbonCarbon;
use Illuminate\Support\Facades\DB;
use Response;

class EleicoesController extends Controller {

	private $model;
	private $zona_id;
	private $ele_id;
	private $valoresForm;
	public $naoEnviados;
	public $enviados;
	private $profile_id;
	private $percentualDeNotificados = 0;
	private $log;


	public function __construct()
	{
		if(is_null($this->model)){
			$this->model = new Eleicao();
			$this->log = new Log();
			//$this->id = Auth::user()->id;
			if(Auth::user()){
				$this->profile_id = Auth::user()->profile_id;
				$this->log = new Log();
				$this->id = Auth::user()->id;
			}
		}
		if(Auth::user()){
			$this->zona_id = Auth::user()->zona_id;
			$this->ele_id = Auth::user()->ele_id;
			$this->log = new Log();
			$this->id = Auth::user()->id;
		}
		$this->enviados = [];
		$this->naoEnviados = [];
	}

	public function index($id){
		$eleicao = $this->model->getById($id);
		
		$ele_id = $eleicao->ele_id;
		$ele_nome = $eleicao->ele_nome;
		$ele_descricao = $eleicao->ele_descricao;
		$ele_logo = $eleicao->ele_logo;
		$ele_qtdVotosCandidatos = $eleicao->ele_qtdVotosCandidatos;
		$ele_tempo = $eleicao->ele_tempo;
		$ele_categoriaEleitor = $eleicao->ele_categoriaEleitor;
		$ele_qtdBu = $eleicao->ele_qtdBu;
		$ele_qtdSessoes = $eleicao->ele_qtdSessoes;
		$ele_horaInicio = Functions::convertDateTime($eleicao->ele_horaInicio);
		$ele_horaTermino = Functions::convertDateTime($eleicao->ele_horaTermino);
		$ele_tipo = $eleicao->ele_tipo;
		$exibir_branco = $eleicao->exibir_branco;
		$exibir_nulo = $eleicao->exibir_nulo;
		$enviar_comprovante = $eleicao->enviar_comprovante;
		$ele_alias = $eleicao->alias;

	
		Session::forget('ele_id');
		Session::put('ele_id', $eleicao->ele_id);
		
		return view('eleicao.pg-eleicao', compact('ele_id',
													'ele_nome',
													'ele_nome_zona',
													'ele_descricao',
													'ele_horaInicio',
													'ele_horaTermino',
													'ele_qtdVotosCandidatos',
													'ele_qtdBu',
													'ele_qtdZeresima',
													'ele_logo',
													'ele_tempo',
													'ele_categoriaEleitor',
													'ele_qtdSessoes',
													'ele_tipo',
													'exibir_branco',
													'exibir_nulo',
													'enviar_comprovante',
													'ele_alias'));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function lista()
	{	
		$listaEleicoes = Eleicao::listar();
		$listaEleicoes->setPath("");
		return view('eleicao.lista-eleicoes', compact('listaEleicoes'));
	}

	public function add()
	{
		$ele_id = '';
		$ele_nome = '';
		$ele_nome_zona = '';
		$ele_descricao = '';
		$ele_horaInicio = '';
		$ele_horaTermino = '';
		$ele_qtdVotosCandidatos = '';
		$ele_qtdBu = '';
		$ele_qtdZeresima = '';
		$ele_logo = '';
		$ele_tempo = '';
		$ele_categoriaEleitor = '';
		$ele_qtdSessoes = '';
		$ele_tipo = 1;
		$exibir_branco = null;
		$exibir_nulo = null;
		$enviar_comprovante = null;
		$exibir_sequenciaVotos = null;
		$habilitar_exclusao = null;
		$aceitar_email_duplicado = null;

		return view('eleicao.add-eleicao', compact('ele_id',
												   'ele_nome',
														 'ele_nome_zona',
														 'ele_descricao',
														 'ele_horaInicio',
														 'ele_horaTermino',
														 'ele_qtdVotosCandidatos',
														 'ele_qtdBu',
														 'ele_qtdZeresima',
														 'ele_logo',
														 'ele_tempo',
														 'ele_categoriaEleitor',
														 'ele_qtdSessoes',
														 'ele_tipo',
														 'exibir_branco',
														 'exibir_nulo',
														 'enviar_comprovante',
														 'exibir_sequenciaVotos',
														 'habilitar_exclusao',
														 'aceitar_email_duplicado'
													));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){}
	// Mover para classe Util!
	/** [formatarDataPhpMySql description] */
	private function formatarDataDateTime($data)
	{
		$retorno = false;

		if(strlen($data))
		{
		   $data    = str_replace("/","-",$data);
		   $retorno = date('Y-m-d H:i:s', strtotime($data) );
		}
		return $retorno;
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		//
		$valoresForm = array_map('trim', Input::all());
		$valoresForm["ele_horaInicio"]  = $this->formatarDataDateTime($valoresForm["ele_horaInicio"]);
		$valoresForm["ele_horaTermino"] = $this->formatarDataDateTime($valoresForm["ele_horaTermino"]);
		$valoresForm['alias'] = Functions::aliasGenerate($valoresForm['ele_nome']);
		$valoresForm['ele_tipo'] = (int) $valoresForm['ele_tipo'];
		$valoresForm['exibir_branco'] = (int) $valoresForm['exibir_branco'];
		$valoresForm['exibir_nulo'] = (int) $valoresForm['exibir_nulo'];
		$valoresForm['enviar_comprovante'] = (int) $valoresForm["enviar_comprovante"];
		$valoresForm['exibir_sequenciaVotos'] = (int) $valoresForm['exibir_sequenciaVotos'];
		$valoresForm['habilitar_exclusao'] = (int) $valoresForm['habilitar_exclusao'];
		$valoresForm['aceitar_email_duplicado'] = (int) $valoresForm['aceitar_email_duplicado'];
		
		
		
		$arquivo = Input::file('ele_logo');
		$valoresForm["ele_logo"] = $this->uploadImagem($arquivo);
		
		$msgRetorno = 'Houve erro na inclusão do registro';
		if(Eleicao::create($valoresForm))
		{
			$msgRetorno = 'Inclusão efetuada com sucesso!';
		}
		return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));
	}
	
	public function end()
	{
		Session::forget('dataEleicao');
		Session::flush();
		
		$opcaoEmail= $this->model->verificaOpcaoComprovanteEmail($this->ele_id);

		if($opcaoEmail == 1){
			return view('eleicao.fim');
		} else if($opcaoEmail== 0){
			return view('eleicao.fim-Alt');
		}
		
	}

	private function uploadImagem($arquivo)
	{
		$arquivo = array('image' => $arquivo);

		$validacao = array('image' => 'required',);

		$validador = Validator::make($arquivo, $validacao);

		if ($validador->fails())
		{
			return Redirect::to('add.eleicoes')->withInput()->withErrors($validador);
		}
		else
		{
			// checking file is valid.
			if (Input::file('ele_logo')->isValid())
			{
				$destino = "uploads/imgs/eleicao/"; // upload path

				File::makeDirectory($destino, $mode = 0777, true, true);

				$extensaoArquivo = Input::file('ele_logo')->getClientOriginalExtension();
				$nomeArquivo = rand(11111,99999).'ele_logo.'.$extensaoArquivo; // renameing image

				Input::file('ele_logo')->move($destino, $nomeArquivo); // uploading file to given path
				return $destino. $nomeArquivo;
			}
			else
			{
				// sending back with error message.
				Session::flash('Erro', 'Upload inválido de arquivo');
				return Redirect::to('add.eleicoes');
			}

		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$eleicao = $this->model->getById($id);
		$ele_id = $eleicao->ele_id;
		$ele_nome = $eleicao->ele_nome;
		$ele_descricao = $eleicao->ele_descricao;
		$ele_logo = $eleicao->ele_logo;
		$ele_qtdVotosCandidatos = $eleicao->ele_qtdVotosCandidatos;
		$ele_tempo = $eleicao->ele_tempo;
		$ele_categoriaEleitor = $eleicao->ele_categoriaEleitor;
		$ele_qtdBu = $eleicao->ele_qtdBu;
		$ele_qtdSessoes = $eleicao->ele_qtdSessoes;
		$ele_horaInicio = Functions::convertDateTime($eleicao->ele_horaInicio);
		$ele_horaTermino = Functions::convertDateTime($eleicao->ele_horaTermino);
		$ele_tipo = $eleicao->ele_tipo;
		$exibir_branco = $eleicao->exibir_branco;
		$exibir_nulo = $eleicao->exibir_nulo;
		$enviar_comprovante = $eleicao->enviar_comprovante;
		$exibir_sequenciaVotos = $eleicao->exibir_sequenciaVotos;
		$habilitar_exclusao = $eleicao->habilitar_exclusao;
		$aceitar_email_duplicado = $eleicao->aceitar_email_duplicado;
		
		return view('eleicao.add-eleicao', compact('ele_id',
												   'ele_nome',
												   'ele_nome_zona',
												   'ele_descricao',
											 'ele_horaInicio',
											 'ele_horaTermino',
											'ele_qtdVotosCandidatos',
											'ele_qtdBu',
											'ele_qtdZeresima',
											'ele_logo',
											'ele_tempo',
											'ele_categoriaEleitor',
											'ele_qtdSessoes',
											'ele_tipo',
											'exibir_branco',
											'exibir_nulo',
											'enviar_comprovante',
											'exibir_sequenciaVotos',
											'habilitar_exclusao',
											'aceitar_email_duplicado'
										));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$valoresForm = array_map('trim', Input::all());
		$eleicao = $this->model->get($valoresForm['ele_id'])[0];
		$eleicao->ele_nome = $valoresForm["ele_nome"];
		$eleicao->ele_descricao = $valoresForm["ele_descricao"];
		$eleicao->ele_qtdVotosCandidatos = $valoresForm["ele_qtdVotosCandidatos"];
		$eleicao->ele_tempo = $valoresForm["ele_tempo"];
		$eleicao->ele_categoriaEleitor = $valoresForm["ele_categoriaEleitor"];
		$eleicao->ele_qtdBu = $valoresForm["ele_qtdBu"];
		$eleicao->ele_qtdSessoes = $valoresForm["ele_qtdSessoes"];
		$eleicao->ele_tipo = (int) $valoresForm["ele_tipo"];
		$eleicao->exibir_branco = (int) $valoresForm["exibir_branco"];
		$eleicao->exibir_nulo = (int) $valoresForm["exibir_nulo"];
		$eleicao->enviar_comprovante = (int) $valoresForm["enviar_comprovante"];
		$eleicao->exibir_sequenciaVotos = (int) $valoresForm["exibir_sequenciaVotos"];
		$eleicao->habilitar_exclusao = (int) $valoresForm["habilitar_exclusao"];
		$eleicao->aceitar_email_duplicado = (int) $valoresForm['aceitar_email_duplicado'];

		// $eleicao->alias = Functions::aliasGenerate($valoresForm['ele_nome']);

		$eleicao->ele_horaInicio  = $this->formatarDataDateTime($valoresForm["ele_horaInicio"]);
		$eleicao->ele_horaTermino = $this->formatarDataDateTime($valoresForm["ele_horaTermino"]);

		$arquivo = Input::file('ele_logo');
		if($arquivo){
			$eleicao->ele_logo = $this->uploadImagem($arquivo);
		}

		$msgRetorno = 'Houve erro na alteração do registro';

		if($eleicao->save())
		{
			$msgRetorno = 'Alteração efetuada com sucesso!';
		}
		return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$eleicao = Eleicao::findOrFail($id);

		$msgRetorno = 'Houve erro na Exclusão do registro';

		if($eleicao->delete())
		{
			$msgRetorno = 'Exclusão efetuada com sucesso!';
		}

	   return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));

	}

	public function verificarEleicaoEncerrada()
	{

		$retorno = false;
		if($this->ele_id):
			$retorno = $this->model->verificarEleicaoEncerrada($this->ele_id);
			return response()->json(['resp' => $retorno]);
		endif;
	}

	public function salvarVotos()
	{
		ini_set("auto_detect_line_endings", true);
		$valoresForm = Input::all();
		$votos = $valoresForm['votos'];
		$cargo = $valoresForm['cargo'];
		$eleicao = CargoModel::getEleicaoByCargo($cargo[0]);
		$modelVoto = new Voto();
		$result = $modelVoto->addVoto($votos, $cargo);

		if(!$result){
			$data = [
				'msg' => 'Erro ao salvar seu votos, tente novamente'
			];
			return view('lista.candidatos.eleitor', with($data));
		}
		$opcaoEmail = $this->model->verificaOpcaoComprovanteEmail($this->ele_id);
		if($opcaoEmail == 1){
			$this->enviarEmailConfirmaçãoVotação();
		}
		

		return Redirect::route('fim.voto', ['alias' => $eleicao->alias]);
	}


	public function gerarCarta($idEleicao = null)
	{
		if(is_null($idEleicao)){
			$listaEleicoes = Eleicao::orderBy('ele_nome')->where('ele_enviar', '=', 1)->get();
		} else {
			$listaEleicoes = Eleicao::orderBy('ele_nome')->where('ele_id', '=', $idEleicao)->where('ele_enviar', '=', 1)->get();
		}

		if($listaEleicoes){
			foreach ($listaEleicoes as $eleicao) {
				return $this->imprimir($eleicao->ele_id);
			}
		}
	}

	public function enviarEmails($letra, $ele_id)
	{
		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);

		
		ini_set("auto_detect_line_endings", true);
		if ($this->profile_id == 2){
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)){
			$model_eleicao = new Eleicao();;
			$dadosEleicao = $model_eleicao->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}

		$arrLetra = Functions::alfabeto($letra);
		if ($letra == $arrLetra['total']){
			return json_encode(['enviados' => $this->enviados, 'naoEnviados' => $this->naoEnviados]);
		}
		$eleitorModel = new Eleitores();
		$listaEleitores = $eleitorModel->eleitorPorLetra($arrLetra['letra'], $ele_id);
		$letra++;
		$this->bodyAll = '';
		$body = View::make('eleicao.base-envio')->render();
		$sendMailController = new SendMailController();
		if(count($listaEleitores) > 0){
			foreach ($listaEleitores as $eleitor) {
				
				$logo = $dadosEleicao->ele_logo;
				if (!file_exists($logo)) {
					$logo = '';
				} else {
					$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$dadosEleicao->ele_logo.'" width="200" />';
				}
				$objEleitor = new Eleitor();
				$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf) . $ele_id . $eleitor->id), 0, 6);
				//$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make(preg_replace('/[^0-9]/', '', $eleitor->cpf))]);
				$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);
				
				/*	Retirado para as eleições do condominio;*/
				// $this->body = View::make('eleicao.base-envio')->render();
				// $this->body = str_replace('[logo]', $logo, $this->body);
				// $this->body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $this->body);
				// $this->body = str_replace('[nome]', $eleitor->name, $this->body);
				// $this->body = str_replace('[dataInicio]', Functions::convertDateTime($dadosEleicao->ele_horaInicio), $this->body);
				// $this->body = str_replace('[dataTermino]', Functions::convertDateTime($dadosEleicao->ele_horaTermino), $this->body);
				// $this->body = str_replace('[login]', $eleitor->matricula, $this->body);
				// $this->body = str_replace('[senha]', $newPass, $this->body);
				// $this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);
				// $this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);

				$objEle = [
					"tipo" => "envio-base",
					"logo" => $logo,
					"ele_nome" => $dadosEleicao->ele_nome,
					"eleitor_name" => $eleitor->name,
					"ele_horaInicio" => Functions::convertDateTime($dadosEleicao->ele_horaInicio),
					"ele_horaTermino" => Functions::convertDateTime($dadosEleicao->ele_horaTermino),
					"eleitor_matricula" => $eleitor->matricula,
					"newPass" => $newPass,
					"eleicao_alias" => $dadosEleicao->alias,
				];
				if($eleitor->email == "teste981390@gmail.com"){die("teste");}
				// $this->bodyAll .= $this->body;
				if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
					if($sendMailController->config($dadosEleicao->ele_nome,'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle)){
						$this->enviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					} else {
						$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					}
				}
				
			}
		}

		if($this->profile_id == 2){
			echo url("notificar-eleitores/letra/$letra");
		} else {
			echo url("notificar-eleitores/letra/$letra/idEleicao/$ele_id");
		}
	}

	/*
		Função só envia para um email para o eleitor

	*/
	
	
	public function enviarEmailEleitor($idEleitor, $idEleicao)
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];

		$operacao = 'RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL';
		$dados = [ 'Nome'=>$eleitor->name, 'Email' =>$eleitor->email ];
		$idRegistro = $idEleicao;
		$idUsuario = $this->id;
		
		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		
		
		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';

		//$body = View::make('eleicao.base-envio')->render();
		// $body = str_replace('[logo]', $logo, $body);
		// $body = str_replace('[eleicao]', $eleicao->ele_nome, $body);
		// $body = str_replace('[nome]', $eleitor->name, $body);
		// $body = str_replace('[dataInicio]', Functions::convertDateTime($eleicao->ele_horaInicio), $body);
		// $body = str_replace('[dataTermino]', Functions::convertDateTime($eleicao->ele_horaTermino), $body);
		// $body = str_replace('[login]', $eleitor->matricula, $body);
		// $body = str_replace('[senha]', $newPass, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);

		$objEle = [
			"tipo" => "envio-base",
			"logo" => $logo,
			"ele_nome" => $eleicao->ele_nome,
			"eleitor_name" => $eleitor->name,
			"ele_horaInicio" => Functions::convertDateTime($eleicao->ele_horaInicio),
			"ele_horaTermino" => Functions::convertDateTime($eleicao->ele_horaTermino),
			"eleitor_matricula" => $eleitor->matricula,
			"newPass" => $newPass,
			"eleicao_alias" => $eleicao->alias,
		];

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config("Redefinição de acesso - ".$eleicao->ele_nome, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}
	}

	public function enviarEmailReenvioSenha($idEleitor, $idEleicao, $valoresForm)
	{

		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];
		
		$operacao = 'RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL';

		$dados = [ 'Nome'=>$valoresForm['nome'], 'Email' =>$valoresForm['email'], 'Matrícula' => $valoresForm['matricula'] , 'CPF' => $valoresForm['cpf'], 'Telefone' => $valoresForm['telefone']];
		$idRegistro = $idEleicao;
		$idUsuario = $idEleitor;
		
		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		
		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';

		//$body = View::make('eleicao.base-envio')->render();
		// $body = str_replace('[logo]', $logo, $body);
		// $body = str_replace('[eleicao]', $eleicao->ele_nome, $body);
		// $body = str_replace('[nome]', $eleitor->name, $body);
		// $body = str_replace('[dataInicio]', Functions::convertDateTime($eleicao->ele_horaInicio), $body);
		// $body = str_replace('[dataTermino]', Functions::convertDateTime($eleicao->ele_horaTermino), $body);
		// $body = str_replace('[login]', $eleitor->matricula, $body);
		// $body = str_replace('[senha]', $newPass, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);

		$objEle = [
			"tipo" => "envio-base",
			"logo" => $logo,
			"ele_nome" => $eleicao->ele_nome,
			"eleitor_name" => $eleitor->name,
			"ele_horaInicio" => Functions::convertDateTime($eleicao->ele_horaInicio),
			"ele_horaTermino" => Functions::convertDateTime($eleicao->ele_horaTermino),
			"eleitor_matricula" => $eleitor->matricula,
			"newPass" => $newPass,
			"eleicao_alias" => $eleicao->alias,
		];

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config("Redefinição de acesso - ".$eleicao->ele_nome, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}

	}

	public function enviarEmailEleitor2($idEleitor, $idEleicao)
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];
		
		$operacao = 'RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL';
		$dados = [ 'Nome'=>$eleitor->name, 'Email' =>$eleitor->email ];
		$idRegistro = $idEleicao;
		$idUsuario = $this->id;

		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		$newPass = preg_replace("/[^0-9]/", rand(0,9), $newPass); 


		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';

		//$body = View::make('eleicao.base-envio')->render();
		// $body = str_replace('[logo]', $logo, $body);
		// $body = str_replace('[eleicao]', $eleicao->ele_nome, $body);
		// $body = str_replace('[nome]', $eleitor->name, $body);
		// $body = str_replace('[dataInicio]', Functions::convertDateTime($eleicao->ele_horaInicio), $body);
		// $body = str_replace('[dataTermino]', Functions::convertDateTime($eleicao->ele_horaTermino), $body);
		// $body = str_replace('[login]', $eleitor->matricula, $body);
		// $body = str_replace('[senha]', $newPass, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);
		// $body = str_replace('[alias]', $eleicao->alias, $body);

		$objEle = [
			"tipo" => "envio-base",
			"logo" => $logo,
			"ele_nome" => $eleicao->ele_nome,
			"eleitor_name" => $eleitor->name,
			"ele_horaInicio" => Functions::convertDateTime($eleicao->ele_horaInicio),
			"ele_horaTermino" => Functions::convertDateTime($eleicao->ele_horaTermino),
			"eleitor_matricula" => $eleitor->matricula,
			"newPass" => $newPass,
			"eleicao_alias" => $eleicao->alias,
		];

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config("Redefinição de acesso - ".$eleicao->ele_nome,'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}
	}


	public function imprimir($ele_id)
	{
		if (!empty($ele_id)){
			$dadosEleicao = $this->model->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}
		$modelEleitor = new Eleitores();
		$listEleitores = $modelEleitor->getForSend($ele_id);
		$lastSend = $modelEleitor->lastSend($ele_id);
		$continue = true;

		$naoEnviados = [];
		if($listEleitores && $continue){
			foreach ($listEleitores as $eleitor) {
				$sendMailController = new SendMailController();
				if($sendMailController->isValidEmail($eleitor->email)) {
					$body = View::make('eleicao.base-carta')->render();
					$logo = $dadosEleicao->ele_logo;
					if (!file_exists($logo)) {
						$logo = '';
					}
					$objEleitor = new Eleitor();
					$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $ele_id . $eleitor->id), 0, 6);
					$objEleitor->where('id', '=', $objEleitor->id)->update(['password' => Hash::make($newPass)]);
					$body = str_replace('[logo]', $logo, $body);
					$body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $body);
					$body = str_replace('[nome]', $eleitor->name, $body);
					$body = str_replace('[login]', $eleitor->matricula, $body);
					$body = str_replace('[senha]', $newPass, $body);
				}
			}

		}
		return $this->naoEnviados;
	}

	public function headerMail($email, $replay){
		$headers = 'From: '. $email . "\r\n" .
			'Reply-To: '.$replay . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		return $headers;
	}

	public function votacao($alias)
	{
		$eleicao = $this->model->getByAlias($alias);
		$_SESSION['eleicao'] = $eleicao;

		/** Mantis 16783 - Marcos Antonio */
		$emissaoZeresima = $this->model->verificarEmissaoZeresima($eleicao->ele_id);
		
		$dataHoje = strtotime(date('Y-m-d H:i:s')); 
		$dataEleicao = strtotime($eleicao->ele_horaInicio);
		$diferencaEntreDatasEmSegundos = $dataEleicao - $dataHoje;
		if($diferencaEntreDatasEmSegundos > 0){
			$tempoParaIniciarAEleicaoEmMinutos = round(($diferencaEntreDatasEmSegundos/60), 2);
		}else{
			$tempoParaIniciarAEleicaoEmMinutos = 0;
		}
		/** FIM Mantis 16783 - Marcos Antonio */
		
		return view('auth.login', compact('eleicao', 'tempoParaIniciarAEleicaoEmMinutos', 'emissaoZeresima'));
	}

	public function enviarEmailConfirmaçãoVotação()
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();

		$eleicao = $model_eleicao->get(Auth::user()->ele_id)[0];
		$eleitor = Auth::user();

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';

		$msgCorpo = 'Agradecemos pela sua participação no processo eleitoral. <br /> Atenciosamente, Comissão Eleitoral';

		// $body = View::make('eleicao.confirmacao-envio')->render();
		// $body = str_replace('[logo]', $logo, $body);
		// $body = str_replace('[eleicao]', $eleicao->ele_nome, $body);
		// $body = str_replace('[nome]', $eleitor->name, $body);
		// $body = str_replace('[corpo]', $msgCorpo, $body);

		$objEle = [
			"tipo" => "confirmacao-envio",
			"logo" => $logo,
			"ele_nome" => $eleicao->ele_nome,
			"eleitor_name" => $eleitor->name,
			"msgCorpo" => $msgCorpo,
		];

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			$sendMailController->config("COMPROVANTE DE VOTAÇÃO - ".$eleicao->ele_nome, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle);
			//$sendMailController->enviar();
		}
	}

	
	public function linkReenviarsenhaFora() {
		$hoje = Carbon::now();
		$sql = "SELECT * FROM eleicao where 
				(day(ele_horaInicio) = ".$hoje->day." and month(ele_horaInicio) = ".$hoje->month.")
				or (day(ele_horaTermino) = ".$hoje->day." and month(ele_horaTermino) = ".$hoje->month.")";
		$eleicoes = DB::select($sql);

		foreach ($eleicoes as $value) {
			if (Carbon::parse($value->ele_horaInicio)->format('d/m/Y') == Carbon::parse($value->ele_horaTermino)->format('d/m/Y')) {
				$value->dataInicio = Carbon::parse($value->ele_horaInicio)->format('d/m/Y - H:i')." às ".Carbon::parse($value->ele_horaTermino)->format('H:i');
			} else {
				$value->dataInicio = Carbon::parse($value->ele_horaInicio)->format('d/m/Y - H:i')." até ".Carbon::parse($value->ele_horaTermino)->format('d/m/Y - H:i');
			}
		}
		return view('auth.reenviarSenhaFora', compact('eleicoes'));
	}

	public function linkReenviarsenha($alias) {
		$eleicao = $this->model->getByAlias($alias);
		$_SESSION['eleicao'] = $eleicao;
		return view('auth.reenviarSenha', compact('eleicao'));
	}

	public function salvarReenviarsenha() {
		$emailCorreto = "";
		$valoresForm = array_map('trim', Input::all());
		$query = Eleitor::where([
			'ele_id' => $valoresForm['ele_id'],
			'email' => $valoresForm['email'],
			'cpf' => $valoresForm['cpf']
	
		]);
		
		$eleitor = $query->first();	
	
		if (count($eleitor) == 1) {
			

			if($eleitor->vote == 1)
			{
				$resultado = "voto_confirmado";

			} else {
				
				if($eleitor->reenvioSenha == 1)
				{
				
					$resultado = "reenvio_confirmado";

				} else {
					
					Eleitor::where('id', '=', $eleitor->id)->update(['reenvioSenha' => '1']);
					$this->enviarEmailReenvioSenha($eleitor->id, $valoresForm['ele_id'], $valoresForm);
					$resultado = "envia_email";
				}
			
			}
		
		} else {
			
			$consulta1 = Eleitor::where(['ele_id' => $valoresForm['ele_id'], 'cpf' => $valoresForm['cpf']])->first();
			$consulta2 = Eleitor::where(['ele_id' => $valoresForm['ele_id'],'email' => $valoresForm['email'], 'cpf' => $valoresForm['cpf']])->first();
			
			$nomeEmailMatricula = count($consulta1) > 0 ? true : false;
			$emailNomeEmailMatricula = count($consulta2) > 0 ? true : false;

			if ($nomeEmailMatricula && !$emailNomeEmailMatricula) {
				// pop-up informando o email e recomenendando entrar em contato com a Comissão eleitoral para atualizar o email
				$resultado = "email_incorreto";
				$emailCorreto = $consulta1->email;
				// $this->enviarEmailReenvioSenhaSuporteBisa($valoresForm, 'O usuário abaixo tentou criar uma nova senha, mas o e-mail informado no formulário não é o mesmo cadatrado no banco de dados.');
			} else {
				// pop-up informando que á inconsistências nos dados e entrar em contato com a Comissão eleitoral
				$resultado = "inconsistencia";
				// $this->enviarEmailReenvioSenhaSuporteBisa($valoresForm, 'O usuário abaixo tentou criar uma nova senha, mas existem inconsistências nos campos informados.');

			}
		}
		return json_encode(['resultado' => $resultado, 'emailCorreto' => $emailCorreto]);
	}

	/**
	 * O método abaixo é usado quando o eleitor esqueceu sua senha.
	 * Será enviado para o contato@bisavoto.com.br um email que o eleitor X alterou sua senha
	 */
	public function enviarEmailReenvioSenhaSuporteBisa($eleitor, $msg) {
		$sendMailController = new SendMailController();

		// $body = View::make('eleicao.reenvio-senha')->render();
		// $body = str_replace('[msg]', $msg, $body);
		// $body = str_replace('[eleicao]', $eleitor['ele_nome'], $body);
		// $body = str_replace('[nome]', $eleitor['nome'], $body);
		// $body = str_replace('[email]', $eleitor['email'], $body);
		// $body = str_replace('[matricula]', $eleitor['matricula'], $body);
		// $body = str_replace('[cpf]', $eleitor['cpf'], $body);
		// $body = str_replace('[telefone]', $eleitor['telefone'], $body);

		$objEle = [
			"tipo" => "reenvio-senha",
			"msg" => $msg,
			"ele_nome" =>  $eleitor['ele_nome'],
			"eleitor_name" => $eleitor['nome'],
			"email" => $eleitor['email'],
			"matricula" => $eleitor['matricula'],
			"cpf" => $eleitor['cpf'],
			"telefone" => $eleitor['telefone'],
		];


		$emailDestino = "desenvolvimento6@bisa.com.br";

		if (filter_var(trim($emailDestino), FILTER_VALIDATE_EMAIL)) {
			$sendMailController->config("ALTERAÇÃO DE SENHA - ".$eleitor['ele_nome'], 'contato@bisavoto.com.br', 'Bisa Voto', trim($emailDestino), $objEle);
			//$sendMailController->enviar();
		}
	}

	public function enviarEmailsEleitores(Request $request, $ele_id){
		$optionEmail = $_GET['optionEmail'];
		$optionPassword = $_GET['optionPassword'];
		$opcaoSenha = 0;

		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);


		ini_set("auto_detect_line_endings", true);
		if ($this->profile_id == 2){
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)){
			$model_eleicao = new Eleicao();;
			$dadosEleicao = $model_eleicao->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}

		
		$eleitorModel = new Eleitores();
		$listaEleitores = $eleitorModel->eleitoresPorIdEleicao($ele_id);

		
		$this->bodyAll = '';
		$sendMailController = new SendMailController();

		if(count($listaEleitores) > 0){

			$dados = [];
			$idRegistro = $ele_id;
			$idUsuario = $this->id;

			foreach ($listaEleitores as $eleitor) {		
				
				$logo = $dadosEleicao->ele_logo;
				if (!file_exists($logo)) {
					$logo = '';
				} else {
					$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$dadosEleicao->ele_logo.'" width="200" />';
				}

				// Gerar senhas 
				$newPass = '';
				if($optionPassword == 'senhaAlfanumerica'){
					$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf) . $ele_id . $eleitor->id), 0, 6);
				} else if($optionPassword == 'senhaNumerica') {
					for($i = 0; $i < 6; $i++){
						$newPass .= mt_rand(0,9);
					}
					$opcaoSenha = 1;
				}

				/** Os IFs e ELSEs a seguir consideram as seguintes situações:
				 *  1- Enviar emails: criar os usuários dos eleitores e enviar a todos via e-mail.
				 *  2- Gerar Senha: apenas criar os usuários sem enviar os e-mails.
				 *  3- Ambos: criar os usuários dos eleitores e enviar a todos via e-mail, com o acréscimo de preencher o campo "serie" com a senha não criptografada.
				 *  Autor: Marcos Antonio.
				 */


				if(($optionEmail == 'enviarEmails' || $optionEmail == 'ambos') && $eleitor->email !== "sememail@sememail.com.br"){ 
					/*	Retirado para as eleições do condominio;*/
					$this->body = View::make('eleicao.base-envio')->render();
					$this->body = str_replace('[logo]', $logo, $this->body);
					$this->body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $this->body);
					$this->body = str_replace('[nome]', $eleitor->name, $this->body);
					$this->body = str_replace('[dataInicio]', Functions::convertDateTime($dadosEleicao->ele_horaInicio), $this->body);
					$this->body = str_replace('[dataTermino]', Functions::convertDateTime($dadosEleicao->ele_horaTermino), $this->body);
					$this->body = str_replace('[login]', $eleitor->matricula, $this->body);
					$this->body = str_replace('[senha]', $newPass, $this->body);
					$this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);
					$this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);

					$objEle = [
						"tipo" => "envio-base",
						"logo" => $logo,
						"ele_nome" => $dadosEleicao->ele_nome,
						"eleitor_name" => $eleitor->name,
						"ele_horaInicio" => Functions::convertDateTime($dadosEleicao->ele_horaInicio),
						"ele_horaTermino" => Functions::convertDateTime($dadosEleicao->ele_horaTermino),
						"eleitor_matricula" => $eleitor->matricula,
						"newPass" => $newPass,
						"eleicao_alias" => $dadosEleicao->alias,
					];
					// $this->bodyAll .= $this->body;
					if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
						if($sendMailController->config($dadosEleicao->ele_nome, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $objEle)){
							$this->enviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
							$objEleitor = new Eleitor();
							$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass), 'opcaoSenha' => $opcaoSenha]);
							$dadosForeach = ['nome' => $eleitor->name, 'email' => $eleitor->email];
							array_push($dados, $dadosForeach);

							if($optionEmail == 'ambos'){
								$objEleitor->where('id', '=', $eleitor->id)->update(['serie' => $newPass]);
						
								$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO DE EMAILS E GERAÇÃO DE SENHA', $idRegistro, $idUsuario, $dadosForeach);
							}else{
								
								$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO DE EMAILS GERAL', $idRegistro, $idUsuario, $dadosForeach);
								
							}
							$dadosForeach = [];
						}else {
							$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
						}
					}else{
						$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					}
				} else if($optionEmail == 'gerarSenha' || $eleitor->email == "sememail@sememail.com.br") {
					$objEleitor = new Eleitor();
					$objEleitor->where('id', '=', $eleitor->id)->update(['serie' => $newPass]);
					$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);
					$dadosForeach = ['nome' => $eleitor->name, 'id' => $eleitor->id ];
					array_push($dados, $dadosForeach);

					$this->log->salvar('RESUMO_MODEL_RESUMO::GERAÇÂO DE SENHA SEM ENVIO DE EMAIL', $idRegistro, $idUsuario, $dadosForeach);
					
					$dadosForeach = [];
				} 

				
			}
			
			if(count($this->naoEnviados)>0){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO DE EMAILS GERAL', $idRegistro, $idUsuario, $this->naoEnviados);
				return json_encode($this->naoEnviados);
			}else{

				return $eleitorModel->percentualDeEleitoresDeNotificados($ele_id);
				
			}

		}
		
		
	}


	public function naoNotificados(Request $request, $ele_id){
		$naoNotificados = json_decode($request->naoEnviados);
		return view("eleitores.lista-eleitores-nao-notificados",  compact("naoNotificados"));
	}

	public function percentualDeEleitoresDeNotificados($ele_id){
		$eleitorModel = new Eleitores();
		return $eleitorModel->percentualDeEleitoresDeNotificados($ele_id);
	}

	
	public function filtroEleicoes()
	{
		$opcao = $_GET["filtro"];
		$listaEleicoes = Eleicao::listarEleicoesFiltro($opcao);
		$listaEleicoes->appends(['filtro' => $opcao]);
		$listaEleicoes->setPath("");
		return view('eleicao.lista-eleicoes', compact('listaEleicoes', 'opcao'));

	}

}
