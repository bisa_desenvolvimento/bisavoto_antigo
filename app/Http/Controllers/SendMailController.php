<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PHPMailer;

class SendMailController extends Controller {

    public $subject;
    public $body;
    public $from;
    public $fromName;
    public $to;
    public $mail;
    
    public function configAWS($subject = null, $body = null, $from = null, $fromName = null, $to = null)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->from = $from;
        $this->fromName = $fromName;
        $this->to = $to;

        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        // Set mailer to use SMTP
        $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Host = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = 'AKIA4GCZDZAEWHRDINJL';                 // SMTP username
        $this->mail->Password = 'BB3mzUgA7zQRGuso3JigmrtbgC8ZPsgu+mAZyFCpy0VQ';                           // SMTP password
        $this->mail->Port = 587;

        /*
        $this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Host = 'smtp.zoho.com';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = 'conectividade@conectividade.inf.br';                 // SMTP username
        $this->mail->Password = 'c0n3ct1v1d4d3';                           // SMTP password
        $this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = 465;
        */

        $this->mail->From = $this->from;
        $this->mail->FromName = $this->fromName;
        $this->mail->Subject = $this->subject;
        $this->mail->Body    = $this->body;
        $this->mail->addAddress($to);
        $this->mail->isHTML(true);
        $this->mail->CharSet = 'UTF-8';
    }

    public function enviar()
    {
        if(!$this->mail->send()) {

            //return $this->msgError().' '.$this->mail->ErrorInfo;
            return $this->mail->ErrorInfo;
        } else {
            return $this->msgOk();
        }
    }

    public function msgError()
    {
        return 'Erro ao enviar mensagem!';
    }

    public function msgOk()
    {
        return true;
    }

    public function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


    public function config($subject = null, $from = null, $fromName = null, $to = null, $Obj = null)
    {

        $urlLogo = url('eleicao');

        switch ($Obj['tipo']) {
            case 'confirmacao-envio':
                $msg = "<h3>".$Obj["ele_nome"]."</h3>Olá ".$Obj["eleitor_name"].",<br /><br/>".$Obj['msgCorpo']."<br /><br />Dúvidas: suporte@bisa.com.br";
                break;

            case 'reenvio-senha':
                $msg = "".$Obj["msg"]."<br/><br/>Eleição: ".$Obj['ele_nome']."<br/>Nome: ".$Obj['eleitor_name']."<br/>E-mail: ".$Obj['email']."<br/>Matricula: ".$Obj['matricula']."<br/>CPF:  ".$Obj['cpf']."<br/>Telefone:  ".$Obj['telefone']."<br/>";
                break;

            default:
                $msg = "<h3>".$Obj["ele_nome"]."</h3>Olá ".$Obj["eleitor_name"].",<br /><br/>Inicio da Eleição:".$Obj["ele_horaInicio"]."<br/>Término da Eleição: ". $Obj["ele_horaTermino"]."<br/><br/>Acesse: <a href='".$urlLogo."/".$Obj["eleicao_alias"]."'> ".$urlLogo."/".$Obj["eleicao_alias"]."</a><br/>Login:".$Obj["eleitor_matricula"]."<br/>Senha: ".$Obj["newPass"]."<br/><br/>Dúvidas: suporte@bisa.com.br";
                break;
        }
    
        
        $url = "https://painel.mailgrid.com.br/api/";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "Authorization: Content-Type: application/json",
        "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        // obs.: Não alterar a identação dessa variável pois intefere na sintaxe do heredoc.
        $data = <<<DATA
{"host_smtp": "server04.mailgrid.com.br","usuario_smtp": "bisavoto@bisavoto.com.br", "senha_smtp": "JNLTyZilf9as", "emailRemetente": "$from", "nomeRemetente": "$fromName","emailDestino": ["$to"], "assunto": "$subject",
"mensagem": "$msg"}
DATA;

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $arrayObj = json_decode($resp, true);

        if ($arrayObj["0"]["status"] !== 'MSG ENVIADA') {
            return false;
        }
      
        return true;
        
    }

}
