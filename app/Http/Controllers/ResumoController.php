<?php namespace App\Http\Controllers;

use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Candidato;
use App\Http\Models\Cargo;
use Illuminate\Http\Request;
use App\Http\Models\Resumo as Resumo;
use App\Http\Models\Log as Log;
use App\Http\Models\Eleicao as Eleicao;
use App\Http\Models\Voto as Voto;
use App\Http\Models\Eleitor as Eleitor;
use App\Http\Models\Zonas;
use Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class ResumoController extends Controller {

    private $ele_id;
    private $model;
    private $log;
    private $modelResumo;
    private $html;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Eleicao();
            $this->log = new Log();       
            $this->modelResumo = new Resumo();
        }
        if(Auth::user()){
            $this->zona_id = Auth::user()->zona_id;
            $this->ele_id = Auth::user()->ele_id;
            $this->profile_id = Auth::user()->profile_id;
            $this->id = Auth::user()->id;
        }
    }

	public function index($idEleicao = null)
	{
        $data = $this->getResumo($idEleicao);
        return view('resumo.lista', compact('data'));
	}

    public function getResumo($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }
        $data = $this->modelResumo->lista($idEleicao);
        return $data;
    }

    public function apuracao($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $data = $this->modelResumo->apuracao($idEleicao);
    }

    public function apuracaofinal($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        } else {
			Session::put('ele_id', $idEleicao);
		}
        $objEleicao = $this->model->get($idEleicao)[0];
        
        return view('resumo.apuracaofinal', compact('objEleicao'));
    }

    public function apuracaofinaleleicao($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $totalVotos = $this->contabilizarVotos($idEleicao);

        $votos = [ 'candidatos' => $totalVotos ];

        $aptos = $this->modelResumo->listarAptos($idEleicao);

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Apuração Final Eleição";

        set_time_limit(0);

        //Padrão do Log
        $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO FINAL';
        $idRegistro = $idEleicao;
        $idUsuario = $this->id;
        $dados = array('totalVotos' => $totalVotos, 'aptos' => count($aptos), 'votos' => $votos);
        $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

        return view('resumo.apuracaofinaleleicao', compact('votos', 'eleicao', 'aptos'));
        
        // return PDF::loadView('resumo.apuracaofinaleleicao', compact('votos', 'eleicao'))
        //         // Se quiser que fique no formato a4 retrato: 
        //         ->setPaper('a4')
        //         ->download('resultado_final_eleicao - '.$eleicao->ele_nome.'.pdf');
    }

    public function apuracaofinaleleicaoporzona($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Apuração Final Eleição por Zona";

        $aptos = $this->modelResumo->listarAptos($idEleicao);

        $dados = array();

        $Zonas = new Zonas();
        $Cargos = new Cargo();
        $cargos = $Cargos->listaCargosComVotacao($idEleicao);

        foreach ($cargos as $cargo) {
            $zonas = $Zonas->listaZonasComVotacao($idEleicao, $cargo->car_id);
            $arrayZonas = array();
            foreach ($zonas as $zona) {
                array_push($arrayZonas, $zona);
                $arrayCandidatos = array();
                $votosTotais = $this->modelResumo->resultadoEleicaoPorZona2($idEleicao, $cargo->car_id, $zona->zon_id);
                foreach ($votosTotais as $key => $value) {
                    array_push($arrayCandidatos, $value);
                }
                $zona->candidatos = $arrayCandidatos;
                $zona->votos_branco = $this->modelResumo->getBrancoZona($idEleicao, $zona->zon_id, $cargo->car_id)->votos;
                $zona->votos_nulo = $this->modelResumo->getNuloZona($idEleicao, $zona->zon_id, $cargo->car_id)->votos;
            }
            $cargo->zona = $arrayZonas;
            // $dadosForeach = ['Zonas' => $cargo->zona, 'Votos Totais' => $votosTotais];
            // array_push($dadosZonas, $dadosForeach);
            // $dadosForeach = [];
            $dadosForeach = ['Zonas' => $zona, 'Votos Totais' => $votosTotais];
			array_push($dados, $dadosForeach);
			$dadosForeach = [];
            
        }
        set_time_limit(0);

         //Padrão do Log
         $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO FINAL ELEICAO POR ZONA';
         $idRegistro = $idEleicao;
         $idUsuario = $this->id;
         $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

         return view('resumo.apuracaofinaleleicaoporzona', compact('eleicao', 'aptos', 'cargos'));
    }

    public function relatorioAptos($idEleicao)
    {      
        ini_set('memory_limit', '-1');

        $aptos = $this->modelResumo->listarAptos($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Aptos";

		Session::forget('ele_id');
		Session::put('ele_id', $idEleicao);

        set_time_limit(0);
        
        /** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
        $this->log->salvar('RESUMO_MODEL_RESUMO::RELATORIO DE APTOS', $idEleicao, $this->id, null);
        /** FIM MANTIS 16869 **/

        return view('resumo.relatorioaptos', compact('aptos', 'eleicao'));

        return PDF::loadView('resumo.relatorioaptos', compact('aptos', 'eleicao'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_aptos - '.$eleicao->ele_nome.'.pdf');
    }

    public function relatorioAptosZona($idEleicao)
    {      
        ini_set('memory_limit', '-1');

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Aptos por Zona";

        $Zonas = new Zonas();
        $zonas = $Zonas->listZonas($idEleicao);

        foreach ($zonas as $zona) {
            $zona->aptos = $this->modelResumo->listarAptosPorZona($idEleicao, $zona->zon_id);
        }
        
        set_time_limit(0);
        
        return view('resumo.relatorioaptosporzona', compact('zonas', 'eleicao'));

        return PDF::loadView('resumo.relatorioaptoszona', compact('zonas', 'eleicao'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_aptos - '.$eleicao->ele_nome.'.pdf');
    }

    public function relatorioVotantes($idEleicao)
    {
        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Votantes";
        $aptos = count($this->modelResumo->listarAptos($idEleicao));

        //Padrão do Log
        $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO PARCIAL';
        $idRegistro = $idEleicao;
        $idUsuario = $this->id;
        $dados = array('votantes' => $votantes, 'aptos' => $aptos);
        $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);


        return view('resumo.relatoriovotantes', compact('votantes', 'eleicao', 'aptos'));

        return PDF::loadView('resumo.relatoriovotantes', compact('votantes', 'eleicao', 'aptos'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_votantes.pdf');
    }    

    public function relatorioVotantesPorZona($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Votantes por Zona";
        $votantes = $this->modelResumo->listarVotantesPorZona($idEleicao);

        $votantesPorZona = [];
        $zonaFiltrada = '';

        foreach ($votantes as $votante) {

            if ($votante->Zona != $zonaFiltrada){
                $zonaFiltrada = $votante->Zona;
                $votantesPorZona[$votante->Zona] = [];
            }

            array_push($votantesPorZona[$votante->Zona], $votante);
        }

        return view('resumo.relatoriovotantesporzona', compact('votantesPorZona', 'eleicao'));
    }

    
    public function relatorioOcorrencias($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Ocorr�ncias";
        
        $dados = DB::select("SELECT * FROM log l
                            inner join users u on l.Log_Usuario = u.id
                            where l.Log_IdRegistro = ".$idEleicao." order by l.Log_Data asc ");
        // dd($dados[0]->Log_Operacao);

        // $votantes = $this->modelResumo->listarVotantesPorZona($idEleicao);

        // $votantesPorZona = [];
        // $zonaFiltrada = '';

        // foreach ($votantes as $votante) {

        //     if ($votante->Zona != $zonaFiltrada){
        //         $zonaFiltrada = $votante->Zona;
        //         $votantesPorZona[$votante->Zona] = [];
        //     }

        //     array_push($votantesPorZona[$votante->Zona], $votante);
        // }

        return view('resumo.relatorioocorrencias', compact('eleicao', 'dados'));
    }

    public function relatorioVotantesFinal($idEleicao)
    {
        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relat�rio Votantes Final";

        /** MANTIS 16869 - Melhoria no Relat�rio de Ocorr�ncias Bisavoto **/
        $this->log->salvar('RESUMO_MODEL_RESUMO::RELATORIO DE VOTANTES', $idEleicao, $this->id, null);
        /** FIM MANTIS 16869 **/

        return view('resumo.relatoriovotantesfinal', compact('votantes', 'eleicao'));

        // return PDF::loadView('resumo.relatoriovotantesfinal', compact('votantes', 'eleicao'))
        //         // Se quiser que fique no formato a4 retrato: 
        //         ->setPaper('a4')
        //         ->download('relatorios_de_votantes_final.pdf');
    }

    public function emitirZeresima($idEleicao)
    {
        $eleicao = Eleicao::find($idEleicao);

        if (strtotime($eleicao->ele_horaInicio) > date('Y-m-d H:i:s')){
            Eleitor::where('ele_id', '=', $idEleicao)->update(['vote' => 0]);
            Voto::where('ele_id', '=', $idEleicao)->delete();

            $totalVotos = $this->contabilizarVotos($idEleicao);
            $votos = [ 'candidatos' => $totalVotos ];

            //Padrão do Log
            $operacao = 'RESUMO_MODEL_RESUMO::ZERESIMA';
            $idRegistro = $idEleicao;
            $idUsuario = $this->id;
            $dados = $votos;
            $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

            return PDF::loadView('resumo.zeresima', compact('votos', 'eleicao'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('Zeresima - '.$eleicao->ele_nome.'.pdf');
        }

        echo "Ação não permitida";
    }

    public function contabilizarVotos($idEleicao)
    {
        $totalVotos = array();
        $votosCandidatos = $this->modelResumo->listaEleicao($idEleicao);

        foreach ($votosCandidatos as $votos) {
            $totalVotos[$votos->car_id][] = $votos;
        }

        foreach ($totalVotos as $key => $value) {
            $totalVotos[$key]['branco'] = $this->modelResumo->getBranco($idEleicao, $key);
            $totalVotos[$key]['nulo'] = $this->modelResumo->getNulo($idEleicao, $key);
        }

        return $totalVotos;
    }

    public function resultadoParcial($nomeEleicao, $idEleicao)
    {
        
        $eleicao = $this->model->get($idEleicao)[0];    

        $aptos = $this->modelResumo->listarAptos($idEleicao);
        $totalAptos = count($aptos);

        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $totalVotantes = count($votantes);
        
        $dataAtual = date('d/m/Y');
        $horaAtual = date('H:i:s');

        return view('resumo.resultadoparcial', compact('totalAptos', 'totalVotantes', 'dataAtual', 'horaAtual', 'eleicao'));


    }

    
}
