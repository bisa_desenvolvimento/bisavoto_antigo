<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Models\Eleicao as Eleicao;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	//use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->auth->login($this->registrar->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        if($request->ele_id) {
            $this->validate($request, [
                'matricula' => 'required', 'password' => 'required', 'ele_id' => 'required'
            ]);
            $credentials = $request->only('matricula', 'password', 'ele_id');

            $credentials['password'] = trim($credentials['password']);
            $credentials['matricula'] = trim($credentials['matricula']);

        } else {
            $this->validate($request, [
                'cpf' => 'required', 'password' => 'required',
            ]);
            $credentials = $request->only('cpf', 'password');

            $credentials['cpf'] = trim($credentials['cpf']);
            $credentials['password'] = trim($credentials['password']);
        }



        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            return redirect()->intended($this->redirectPath());
        }

        if(isset($request->ele_id)) {
            return redirect($this->loginPath())
                ->withInput($request->only('matricula', 'ele_id', 'remember'))
                ->withErrors([
                    'matricula' => $this->getFailedLoginMessage(),
                ]);

        } else {
            return redirect($this->loginPath())
                ->withInput($request->only('cpf', 'remember'))
                ->withErrors([
                    'cpf' => $this->getFailedLoginMessage(),
                ]);

        }

    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return 'Dados inválidos.';
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    { 
        $user = $this->auth->user();
        $this->auth->logout();

        if($user->profile_id == 2 || $user->profile_id == 3){// se usuário for "comissão" ou "eleitor", será redirecionado para a tela de login da eleição.
            $modelEleicao = new Eleicao();
            $eleicao = $modelEleicao->getById($user->ele_id);
            $eleAlias = $eleicao->alias;
            $urlEleicao = url('eleicao')."/$eleAlias";
            
            return redirect($urlEleicao);
        }else{
            return redirect()->action('HomeController@index');
        }
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : $_SERVER['HTTP_REFERER'];
    }
}
